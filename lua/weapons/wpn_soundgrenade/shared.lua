if( SERVER ) then
    AddCSLuaFile( "shared.lua" );
end
 
if( CLIENT ) then
    SWEP.PrintName = "Sound Grenade";
    SWEP.Slot = 4;
    SWEP.SlotPos = 4;
    SWEP.DrawAmmo = true;
    SWEP.DrawCrosshair = false;
end
 
if CLIENT then
language.Add("wpn_soundgrenade", "Sound Grenade")
end
 
SWEP.Base               = "weapon_base"
SWEP.Category          = "Stealth Tools"
SWEP.Author         = "Max Shadow"
SWEP.Instructions   = "Left Click: Throw far. \nRight Click: Throw close."
SWEP.Contact        = ""
SWEP.Purpose        = "Emits a gunshot sound to attract nearby enemies."
 
SWEP.HoldType = "grenade"
SWEP.ViewModelFOV   = 54
SWEP.ViewModelFlip  = false
 
SWEP.Spawnable          = true
SWEP.AdminSpawnable     = true
SWEP.UseHands			= true

SWEP.ViewModel			= "models/weapons/c_grensnd.mdl"
SWEP.WorldModel			= "models/weapons/w_grensnd.mdl"

game.AddAmmoType( {	name = "sm_soundgrenade", maxcarry = 3} )
if ( CLIENT ) then language.Add( "sm_soundgrenade_ammo", "Sound Grenade" ) end

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "sm_soundgrenade"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo		= "none"

--When the script loads, the sound ''Metal.SawbladeStick'' will be precached, 
--and a local variable with the sound name created.
--local ShootSound = Sound("Metal.SawbladeStick")
--killicon.AddFont( "wpn_soundgrenade", "HalfLife2", "4", Color( 255, 80, 0, 255 ) )

function SWEP:Initialize()
	self:SetWeaponHoldType(self.HoldType)
end

function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
	-- Set us up the texture
	surface.SetDrawColor( color_transparent )
	surface.SetTextColor( 255, 220, 0, alpha )
	surface.SetFont( "CreditsLogo" )
	local w, h = surface.GetTextSize("k")
	
	-- Draw that mother
	surface.SetTextPos( x + ( wide / 2 ) - ( w / 2 ),
						y + ( tall / 2 ) - ( h / 2 ) )
	surface.DrawText("k")
	
	self:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
end

function SWEP:Reload()
end
 
function SWEP:Think()
end

function SWEP:Deploy()
	self:SendWeaponAnim(ACT_VM_DRAW);
	return true
end

function SWEP:Holster(selwep)
	if timer.Exists("anim_timer"..tostring(self)) then
		timer.Remove("anim_timer"..tostring(self))
	end
	
	return true	
end

function SWEP:ThrowGrenade(force, pos, ang)
	local own = self:GetOwner()
	local ent = ents.Create( "ent_soundgrenade" )
	local Forward = own:GetAimVector()
	local Right = own:GetRight()
	local Up = own:GetUp()
	--ent:SetActive( false )
	ent:SetPos( own:GetShootPos() + Forward * pos.z + Right * pos.x + Up * pos.y)
	ent:SetAngles( own:EyeAngles() + ang )
	ent:SetOwner(own)
	ent:Spawn()
	ent:Activate()
	local phys = ent:GetPhysicsObject()
	if phys then
		phys:ApplyForceCenter(Forward * force + Vector(0,0,400))
		phys:AddAngleVelocity(Vector(200, 600, 100))
	end

	undo.Create("sound_grenade")
	undo.AddEntity( ent )
	undo.SetPlayer( own )
	undo.Finish()

	self:TakePrimaryAmmo( 1 )
	timer.Create( "anim_timer"..tostring(self), 0.5, 1, function()
		if ( own:GetAmmoCount( self:GetPrimaryAmmoType() ) <= 0 ) then 
			own:StripWeapon( self:GetClass() )
		else 
			self:SendWeaponAnim( ACT_VM_DRAW )
		end
	end)
end

function SWEP:PrimaryAttack()
	local own = self:GetOwner()
	if own:GetAmmoCount( self:GetPrimaryAmmoType() ) > 0 and self:GetNextPrimaryFire() < CurTime() then
		own:SetAnimation(PLAYER_ATTACK1)
		self:SendWeaponAnim( ACT_VM_THROW )
		timer.Simple(0.18, function()
			if own:Alive() and own:GetActiveWeapon() == self then
				self:ThrowGrenade(5000, Vector(12, -5, 10), Angle(0,0,-40))
			end
		end)
		self:SetNextPrimaryFire( CurTime() + 1.2 )
	end
end

function SWEP:SecondaryAttack()
	local own = self:GetOwner()
	if own:GetAmmoCount( self:GetPrimaryAmmoType() ) > 0 and self:GetNextSecondaryFire() < CurTime() then
		own:SetAnimation(PLAYER_ATTACK1)
		self:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
		timer.Simple(0.15, function()
			if own:Alive() and own:GetActiveWeapon() == self then
				self:ThrowGrenade(2500, Vector(3, -8, 12), Angle(0,0,30))
			end
		end)
		self:SetNextSecondaryFire( CurTime() + 1.2 )
	end
end

function SWEP:CanPrimaryAttack()
	return true
end

function SWEP:CanSecondaryAttack()
	return true
end
