if( SERVER ) then
    AddCSLuaFile( "shared.lua" );
end
 
if( CLIENT ) then
    SWEP.PrintName = "Stun Baton";
    SWEP.Slot = 0;
    SWEP.SlotPos = 4;
    SWEP.DrawAmmo = false;
    SWEP.DrawCrosshair = false;
end
 
if CLIENT then
language.Add("wpn_stealthstunbaton", "Stun Baton")
end
 
SWEP.Base               = "weapon_base"
SWEP.Category          = "Stealth Tools"
SWEP.Author         = "Max Shadow"
SWEP.Instructions   = "Left Click: Swing."
SWEP.Contact        = ""
SWEP.Purpose        = "Stuns enemies in one hit."
 
SWEP.HoldType = "melee"
SWEP.ViewModelFOV   = 54
SWEP.ViewModelFlip  = false
 
SWEP.Spawnable          = true
SWEP.AdminSpawnable     = true
SWEP.UseHands			= true
   
SWEP.ViewModel      = "models/weapons/c_stthbaton.mdl"
SWEP.WorldModel   = "models/weapons/w_stthbaton.mdl"
   
-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Ammo           = "none"
  
-------------Secondary Fire Attributes-------------------------------------
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
	self:SetWeaponHoldType(self.HoldType)
end

function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
		-- Set us up the texture
		surface.SetDrawColor( color_transparent )
		surface.SetTextColor( 255, 220, 0, alpha )
		surface.SetFont( "CreditsLogo" )
		local w, h = surface.GetTextSize("n")
		
		-- Draw that mother
		surface.SetTextPos( x + ( wide / 2 ) - ( w / 2 ),
							y + ( tall / 2 ) - ( h / 2 ) )
		surface.DrawText("n")
		
		self:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
end
 
function SWEP:Precache()
	
	util.PrecacheSound(Sound( "Weapon_StunStick.Melee_Hit" ))
	util.PrecacheSound("weapons/iceaxe/iceaxe_swing1.wav")
	
end
 
function SWEP:Deploy()
	self:SendWeaponAnim(ACT_VM_DRAW);
    return true;
end
 
function SWEP:ImpactEffect( traceHit )

	local data = EffectData();
	data:SetNormal( traceHit.HitNormal );
	data:SetOrigin( traceHit.HitPos + ( traceHit.HitNormal * 4.0 ) );
	util.Effect( "StunstickImpact", data );

end

function SWEP:MakeSleep(target, sleeptime)
	if SERVER then
	
	local ragent = ents.Create("prop_ragdoll")
	ragent:SetModel(target:GetModel())
	ragent:SetSkin(target:GetSkin())
	ragent:SetMaterial(target:GetMaterial())
	ragent:SetPos(target:GetPos())
	ragent:GetTable().ang2 = target:GetAngles()
	ragent:SetVelocity(target:GetVelocity())
	ragent:SetColor(target:GetColor())
	ragent:Spawn()
	ragent:SetName(target:GetName())
	ragent:SetNWString("sm_NPCclass",target:GetClass())
	ragent:SetNWInt("sm_NPChealth",target:Health())
	ragent:SetNWString("sm_NPCname",target:GetName())
	if IsValid(target:GetActiveWeapon()) then
		ragent:SetNWString("sm_NPCweapon",target:GetActiveWeapon():GetClass())
		local wep = ents.Create("cycler") 
		wep:SetModel( target:GetActiveWeapon():GetModel() )
		wep:SetParent(ragent)
		wep:AddEffects( EF_BONEMERGE )
		wep:Spawn()
	end
	for i = 0,32 do
		ragent:SetBodygroup(i,target:GetBodygroup(i))
	end
	-- Copy bone position
	for i = 0, ragent:GetPhysicsObjectCount() do
		local physobj = ragent:GetPhysicsObjectNum( i )
		if (physobj) and IsValid(physobj)then
			local pos, ang = target:GetBonePosition( target:TranslatePhysBoneToBone( i ) )
			physobj:SetPos(pos)
			physobj:SetAngles(ang)
			physobj:Wake()
			--physobj:EnableMotion(true)
		end
	end
	ragent:SetNWBool("sm_sleepingNPC",true)
	ragent:SetNWFloat("sm_sleepinit",CurTime())
	ragent:SetNWFloat("sm_sleeptime",sleeptime)
	
	if target:IsPlayer() then
		ragent:SetNWEntity("sm_SpecPlayer", target)
		ragent:SetNWInt("sm_NPCarmor",target:Armor())
		ragent:SetNWString("sm_LastWeapon", target:GetPreviousWeapon():GetClass())
		ragent.BackupData = target:GetTable()
		ragent.Weapons = {}
		ragent.Ammo = {}
		for k, v in pairs(target:GetWeapons()) do
			ragent.Weapons[v:GetClass()] = {v:Clip1(), v:Clip2()}
			ragent.Ammo[v:GetPrimaryAmmoType()] = target:GetAmmoCount(v:GetPrimaryAmmoType())
			ragent.Ammo[v:GetSecondaryAmmoType()] = target:GetAmmoCount(v:GetSecondaryAmmoType())
		end
		target:Freeze(true)
		target:DrawViewModel(false)
		target:StripWeapons()
		target:ScreenFade( SCREENFADE.OUT, Color(8,0,16,250), 1, sleeptime )
		target:Spectate(OBS_MODE_CHASE)
		target:SpectateEntity(ragent)
		target:SetNWEntity("sm_SpecRagdoll", ragent)
	else
		target:Remove()
	end
	
	
	timer.Create("wake_time_"..ragent:EntIndex(),sleeptime,1,function()
		if IsValid(ragent) and ragent:GetNWInt("sm_NPChealth") > 0 then -- Spawning the NPC again
			
			if ragent:GetNWString("sm_NPCclass") == "player" then -- It's a player
				target:ScreenFade( SCREENFADE.PURGE, Color(0,0,0,0), 1, sleeptime )
				target:UnSpectate()
				target:Spawn()
				target:StripWeapons()
				target:StripAmmo()
				target:SetPos(ragent:WorldSpaceCenter())
				target:SetAngles(ragent:GetTable().ang2)
				target:SetTable(ragent.BackupData)
				target:SetArmor(ragent:GetNWInt("sm_NPCarmor"))
				target:SetHealth(ragent:GetNWInt("sm_NPChealth"))
				target:SetNWEntity("sm_SpecRagdoll", nil)
				for k, v in pairs(ragent.Weapons) do
					target:Give(k)
					target:GetWeapon(k):SetClip1(v[1])
					target:GetWeapon(k):SetClip2(v[2])
				end
				for k, v in pairs(ragent.Ammo) do
					target:SetAmmo(v, k)
				end
				target:SelectWeapon(ragent:GetNWString("sm_LastWeapon"))
				target:SelectWeapon(ragent:GetNWString("sm_NPCweapon"))
				target:DrawViewModel(true)
				target:Freeze(false)
			else 
				local npc = ents.Create(ragent:GetNWString("sm_NPCclass")) --It's an NPC/Nextbot
				npc:SetPos(ragent:GetPos())
				if !(ragent:GetTable().ang2 == nil) then
					npc:SetAngles(ragent:GetTable().ang2)
				end
				if ragent:GetNWString("sm_NPCweapon") != nil then
					npc:SetKeyValue("additionalequipment",ragent:GetNWString("sm_NPCweapon"))
				end
				npc:SetSkin(ragent:GetSkin())
				npc:Activate()
				npc:Spawn()
				npc:SetModel(ragent:GetModel())
				npc:SetMaterial(ragent:GetMaterial())
				npc:SetColor(ragent:GetColor())
				npc:SetName(ragent:GetNWString("sm_NPCname"))
				npc:SetHealth(ragent:GetNWInt("sm_NPChealth"))
				for i = 0,32 do
					npc:SetBodygroup(i,ragent:GetBodygroup(i))
				end
			end
			ragent:Remove()
		end 
	end)
	
	end
end

function SWEP:PrimaryAttack()
	local own = self:GetOwner()
    if !self:CanPrimaryAttack() then return; end
    self:SetNextPrimaryFire( CurTime() + .5 );
    local trace = own:GetEyeTrace();
    if trace.HitPos:DistToSqr(own:GetShootPos()) <= 5625 then
		self:EmitSound(Sound( "Weapon_StunStick.Melee_Hit" ))
		own:SetAnimation( PLAYER_ATTACK1 );
		self:SendWeaponAnim( ACT_VM_HITCENTER );
		if (trace.Entity:IsPlayer() and cvars.Bool("stealth_stunplayers")) or trace.Entity:GetNWBool("stealth_stealthNPC") then
			self:MakeSleep(trace.Entity, cvars.Number("stealth_sleeptime"))
        else
			bullet = {}
			bullet.Num    = 1
			bullet.Src    = own:GetShootPos()
			bullet.Dir    = own:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 1
			bullet.Damage = 10
			own:FireBullets(bullet)
		end
		self:ImpactEffect( trace );
    else
        own:SetAnimation( PLAYER_ATTACK1 );
        self:SendWeaponAnim( ACT_VM_MISSCENTER );
        self:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")
    end
end

function SWEP:SecondaryAttack()
	return false
end

function SWEP:Reload()
	return false
end

function SWEP:CanPrimaryAttack()
	return true
end

function SWEP:CanSecondaryAttack()
	return false
end
