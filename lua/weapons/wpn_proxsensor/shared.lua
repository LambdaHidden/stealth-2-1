if( SERVER ) then
    AddCSLuaFile( "shared.lua" );
end
 
if( CLIENT ) then
    SWEP.PrintName = "Proximity Sensor";
    SWEP.Slot = 4;
    SWEP.SlotPos = 4;
    SWEP.DrawAmmo = true;
    SWEP.DrawCrosshair = false;
end
 
if CLIENT then
language.Add("wpn_proxsensor", "Proximity Sensor")
end
 
SWEP.Base               = "weapon_base"
SWEP.Category          = "Stealth Tools"
SWEP.Author         = "Max Shadow"
SWEP.Instructions   = "Left Click: Throw far. \nRight Click: Throw close."
SWEP.Contact        = ""
SWEP.Purpose        = "Shows nearby enemies through walls."
 
SWEP.HoldType = "slam"
SWEP.ViewModelFOV   = 54
SWEP.ViewModelFlip  = false
 
SWEP.Spawnable          = true
SWEP.AdminSpawnable     = true
SWEP.UseHands			= true

SWEP.ViewModel			= "models/weapons/c_pxsr.mdl"
SWEP.WorldModel			= "models/weapons/w_pxsr.mdl"

game.AddAmmoType( {	name = "sm_proxsensor", maxcarry = 3} )
if ( CLIENT ) then language.Add( "sm_proxsensor_ammo", "Proximity Sensor" ) end

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "sm_proxsensor"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo		= "none"

function SWEP:Initialize()
	self:SetWeaponHoldType(self.HoldType)
end

function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
	-- Set us up the texture
	surface.SetDrawColor( color_transparent )
	surface.SetTextColor( 255, 220, 0, alpha )
	surface.SetFont( "CreditsLogo" )
	local w, h = surface.GetTextSize("o")
	
	-- Draw that mother
	surface.SetTextPos( x + ( wide / 2 ) - ( w / 2 ),
						y + ( tall / 2 ) - ( h / 2 ) )
	surface.DrawText("o")

	self:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
end

function SWEP:Reload()
end

function SWEP:Think()
end

function SWEP:Deploy()
	self:SendWeaponAnim(ACT_SLAM_THROW_ND_DRAW);
	return true
end

function SWEP:Holster(selwep)
	if timer.Exists("anim_timer"..tostring(self)) then
		timer.Remove("anim_timer"..tostring(self))
	end
	
	return true	
end

function SWEP:ThrowGrenade(force)
	local own = self:GetOwner()
	local ent = ents.Create( "ent_proxsensor" )
	local Forward = own:GetAimVector()
	local Up = own:GetUp()
	--ent:SetActive( false )
	ent:SetPos( own:GetShootPos() + Forward * 8 + Up * -6)
	ent:SetAngles( own:EyeAngles() )
	ent:SetOwner(own)
	ent:Spawn()
	ent:Activate()
	local phys = ent:GetPhysicsObject()
	if phys then
		phys:ApplyForceCenter(Forward * force)
		phys:AddAngleVelocity(Vector(0, 0, -600))
	end

	undo.Create("sound_grenade")
	undo.AddEntity( ent )
	undo.SetPlayer( own )
	undo.Finish()

	self:TakePrimaryAmmo( 1 )
	self:SendWeaponAnim( ACT_SLAM_THROW_THROW_ND2 )
	timer.Create( "anim_timer"..tostring(self), 0.2, 1, function()
		if ( own:GetAmmoCount( self:GetPrimaryAmmoType() ) <= 0 ) then 
			own:StripWeapon( self:GetClass() )
		else 
			self:SendWeaponAnim( ACT_SLAM_THROW_ND_DRAW )
		end
	end)
end

function SWEP:PrimaryAttack()
	local own = self:GetOwner()
	if own:GetAmmoCount( self:GetPrimaryAmmoType() ) > 0 and self:GetNextPrimaryFire() < CurTime() then
		own:SetAnimation(PLAYER_ATTACK1)
		self:SendWeaponAnim( ACT_SLAM_THROW_THROW_ND )
		timer.Simple(0.48, function()
			if own:Alive() and own:GetActiveWeapon() == self then
				self:ThrowGrenade(6000)
			end
		end)
		self:SetNextPrimaryFire( CurTime() + 1.2 )
	end
end

function SWEP:SecondaryAttack()
	local own = self:GetOwner()
	if own:GetAmmoCount( self:GetPrimaryAmmoType() ) > 0 and self:GetNextSecondaryFire() < CurTime() then
		own:SetAnimation(PLAYER_ATTACK1)
		self:SendWeaponAnim( ACT_SLAM_THROW_THROW_ND )
		timer.Simple(0.48, function()
			if own:Alive() and own:GetActiveWeapon() == self then
				self:ThrowGrenade(3000)
			end
		end)
		self:SetNextSecondaryFire( CurTime() + 1.2 )
	end
end

function SWEP:CanPrimaryAttack()
	return true
end

function SWEP:CanSecondaryAttack()
	return true
end
