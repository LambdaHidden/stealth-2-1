AddCSLuaFile()

EFFECT.Material = Material( "stealth/calm.png" )

function EFFECT:Init( data )
	self.Entity = data:GetEntity()
	
	self.StartTime = CurTime()
	self.EndTime = CurTime() + 2
end

function EFFECT:DoRemove()	
	self.ShouldRemove = true
end

function EFFECT:Render()
	if !IsValid(self.Entity) then return end
	
	render.SetMaterial( self.Material )
	--[[self.Height = Lerp( FrameTime() * 5, self.Height, self.EndHeight)
	
	if ( ( self.EndTime - CurTime() ) < 0.5 ) then
		self.Alpha = math.Clamp( Lerp( FrameTime() * 7, self.Alpha, 0 ), 0, 255 )
	end
	
	if IsValid(self.Entity) then
		render.DrawSprite( self.Entity:GetPos() + Vector( 0, 0, 85 ), 16, 16, Color( 255, 255, 255, self.Alpha )  )
	end]]
	self.Width = math.Clamp(72+math.sin((self.StartTime - CurTime())*2)*256, 32, 64)
	self.Height = math.Clamp(math.sin((CurTime() - self.StartTime)*2)*168, 0, 32)
	
	render.DrawSprite( self.Entity:GetPos() + Vector( 0, 0, 85 ), self.Width, self.Height, Color( 255, 255, 255, 255 )  )
end

function EFFECT:Think()
	if IsValid(self.Entity) then
		self:SetPos( self.Entity:GetPos() )
	end
	
	if self.ShouldRemove then return false end
	
	return self.EndTime > CurTime()
end
