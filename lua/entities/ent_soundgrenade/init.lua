AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	
	self:SetModel("models/weapons/w_grensnd.mdl")
	
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:DrawShadow( false )
	self:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	
	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(6.5)
		phys:SetDamping(0.1,5)
	end
	
	self:SetFriction(3)
	
	self.timeleft = CurTime() + 4 -- HOW LONG BEFORE EXPLOSION
end
/*
function ENT:SetupDataTables()
	self:NetworkVar( "Bool", 0, "Active" )
end
*/
function ENT:Think()
	if self.timeleft < CurTime() then
		self:Explosion()
	end
end

function ENT:Explosion()

	if not IsValid(self.Owner) then
		self:Remove()
		return
	end
	
	local effectdata = EffectData()
	effectdata:SetOrigin( self:GetPos() )
	util.Effect( "cball_explode", effectdata, true, true )
	self:EmitSound( "weapons/deagle/deagle-1.wav", 100, 130, 1, CHAN_AUTO )
	self:Remove()
	hook.Run( "MakeNoiseDistraction", self:GetPos(), true )
end
