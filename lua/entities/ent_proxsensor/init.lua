AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	
	self:SetModel("models/weapons/w_pxsr.mdl")
	
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:DrawShadow( false )
	self:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	
	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(6.5)
		phys:SetDamping(0.1,5)
	end
	
	self:SetFriction(3)
	
	self.phase = 1
	self.timeleft = CurTime() + 2 -- HOW LONG BEFORE TURNING ON
	self:Think()
end

function ENT:Think()
	if self.timeleft < CurTime() then
		if self.phase == 1 then self:TurnOn()
		elseif self.phase == 2	then self:Remove() end
	end
end

function ENT:TurnOn()
	self.phase = 2

	if not IsValid(self.Owner) then
		self:Remove()
		return
	end
	
	self:EmitSound( "items/battery_pickup.wav", 100, 80, 1, CHAN_AUTO )

	hook.Run( "TurnOnSensor", self, true )

	self.timeleft = CurTime() + cvars.Number("stealth_sensortime")
end
