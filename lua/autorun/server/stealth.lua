include( "sh_stealth.lua" )
AddCSLuaFile( "sh_stealth.lua" )
resource.AddFile("sound/stealth/alert.wav");
resource.AddFile("sound/stealth/alertmusic.wav");

util.AddNetworkString("stealth_enabled")
util.AddNetworkString("stealth_alerttime")
util.AddNetworkString("stealth_override")
util.AddNetworkString("stealth_stunplayers")
util.AddNetworkString("stealth_keepcorpses")
util.AddNetworkString("stealth_alertondamage")
util.AddNetworkString("stealth_multiplier")
util.AddNetworkString("stealth_minsight")
util.AddNetworkString("stealth_maxsight")
util.AddNetworkString("stealth_movebonus")
util.AddNetworkString("stealth_minhearing")
util.AddNetworkString("stealth_shotrange")
util.AddNetworkString("stealth_suppmultiplier")
util.AddNetworkString("stealth_backuptime")
util.AddNetworkString("stealth_backuprange")
util.AddNetworkString("stealth_maxcorpses")
util.AddNetworkString("stealth_sensortime")
util.AddNetworkString("stealth_sensorrange")
util.AddNetworkString("stealth_sleeptime")
util.AddNetworkString("stealth_general_hearing_range")
util.AddNetworkString("force_enemy_spawns_on_level_load")
util.AddNetworkString("force_activate_enemy_spawners")
util.AddNetworkString("stealth_cheat_no_alert")
util.AddNetworkString("stealth_cloak_allowed")
util.AddNetworkString("stealth_cloak_material")
util.AddNetworkString("stealth_reloadsettings")
util.AddNetworkString("stealth_defaultsettings")
util.AddNetworkString("stealth_clientinitialized")

util.AddNetworkString("NPCAlerted")
util.AddNetworkString("NPCEffect")
util.AddNetworkString("NPCCalmed")
util.AddNetworkString("RemoveNPCfromTable")
util.AddNetworkString("AddNPCtoTable")
util.AddNetworkString("AddSensor")

local npctable = {}
local corpsetable = {}
local npcdied = false

local silent_list = {}
local npc_list = {}
local suppressed_list = {}
local ignoresounds_list = {}

--Add silent stuff here
local silent_default = 
{
	["weapon_physgun"] = true,
	["gmod_tool"] = true,
	["weapon_crossbow"] = true,
	["novoice"] = true,
	["weapon_gascan_limited"] = true,
	["weapon_gascan"] = true,
	["blink_swep"] = true,
	["climb_swep2"] = true,
	["weapon_fists"] = true,
	["weapon_m24sd"] = true,
	["spiderman's_swep"] = true,
	["weapon_medkit"] = true,
	["weapon_minecraft_torch"] = true,
	["laserpointer"] = true,
	["remotecontroller"] = true,
	["m9k_honeybadger"] = true,
	["m9k_mp7"] = true,
	["m9k_harpoon"] = true,
	["m9k_knife"] = true,
	["m9k_machete"] = true,
	["m9k_fists"] = true,
	["m9k_damascus"] = true,
	["m9k_svu"] = true,
	["gdcw_lr300ironsupp"] = true,
	["gdcw_lr300scopesupp"] = true,
	["gdcw_mk14ebr_supp"] = true,
	["weapon_crowbar"] = true,
	["weapon_bugbait"] = true,
	["weapon_frag"] = true,
	["weapon_physcannon"] = true,
	["weapon_slam"] = true,
	["weapon_stunstick"] = true,
	["weapon_chumtoad"] = true,
	["weapon_crossbow_hl"] = true,
	["weapon_crowbar_hl"] = true,
	["weapon_handgrenade"] = true,
	["weapon_tripmine"] = true,
	["weapon_satchel"] = true,
	["weapon_snark"] = true,
	["weapon_barnacle"] = true,
	["weapon_knife"] = true,
	["weapon_penguin"] = true,
	["weapon_pipewrench"] = true,
	["tfa_nmrih_bat"] = true,
	["tfa_nmrih_bcd"] = true,
	["tfa_nmrih_cleaver"] = true,
	["tfa_nmrih_crowbar"] = true,
	["tfa_nmrih_bow"] = true,
	["tfa_nmrih_zippo"] = true,
	["tfa_nmrih_etool"] = true,
	["tfa_nmrih_fireaxe"] = true,
	["tfa_nmrih_fext"] = true,
	["tfa_nmrih_fists"] = true,
	["tfa_nmrih_frag"] = true,
	["tfa_nmrih_fubar"] = true,
	["tfa_nmrih_hatchet"] = true,
	["tfa_nmrih_kknife"] = true,
	["tfa_nmrih_lpipe"] = true,
	["tfa_nmrih_machete"] = true,
	["tfa_nmrih_maglite"] = true,
	["tfa_nmrih_molotov"] = true,
	["tfa_nmrih_pickaxe"] = true,
	["tfa_nmrih_sledge"] = true,
	["tfa_nmrih_spade"] = true,
	["tfa_nmrih_tnt"] = true,
	["tfa_nmrih_welder"] = true,
	["tfa_nmrih_wrench"] = true,
	["tfcss_css_knife"] = true,
	["tfcss_cssfrag"] = true,
	["tfcss_css_c4"] = true,
	["tfcss_css_smoke"] = true,
	["tfcss_css_c4_alt"] = true,
	["tfcss_cssfrag_alt"] = true,
	["tfcss_css_knife_alt"] = true,
	["tfcss_css_smoke_alt"] = true,
	["tfa_csgo_bayonet"] = true,
	["tfa_csgo_butfly"] = true,
	["tfa_csgo_decoy"] = true,
	["tfa_csgo_falch"] = true,
	["tfa_csgo_flash"] = true,
	["tfa_csgo_flip"] = true,
	["tfa_csgo_frag"] = true,
	["tfa_csgo_gut"] = true,
	["tfa_csgo_tackni"] = true,
	["tfa_csgo_incen"] = true,
	["tfa_csgo_karam"] = true,
	["tfa_csgo_ctknife"] = true,
	["tfa_csgo_tknife"] = true,
	["tfa_csgo_m9"] = true,
	["tfa_csgo_molly"] = true,
	["tfa_csgo_pushkn"] = true,
	["tfa_csgo_smoke"] = true,
	["wpn_proxsensor"] = true,
	["wpn_soundgrenade"] = true,
	["wpn_stealthstunbaton"] = true
}

local suppressed_default = 
{
	["tfa_svu"] = true,
	["tfa_wa2000"] = true,
	["tfa_honeybadger"] = true,
	["tfa_mp7"] = true,
	["tfa_mp5sd"] = true,
	["tfa_mp9"] = true,
	["tfcss_tmp"] = true,
	["tfcss_tmp_alt"] = true,
	["tfa_val"] = true,
	["tfa_smg_mp9"] = true,
	["wpn_stealthtranquilizer"] = true
}

local npc_default = 
{
	["npc_combine_s"] = true,
	["npc_metropolice"] = true
}

local ignoresounds_default = 
{
	["BaseCombatCharacter.AmmoPickup"] = true,
	["HealthVial.Touch"] = true,
	["ItemBattery.Touch"] = true
}

--Easy way to disable printing debug garbage to console in release versions; just comment out the print
local function debug_print(str)
	print(str)
end

local function load_files()
	silent_list = table.Copy(silent_default)
	suppressed_list = table.Copy(suppressed_default)
	npc_list = table.Copy(npc_default)
	ignoresounds_list = table.Copy(ignoresounds_default)
	
	local s = file.Read("stealth/silent.txt", "DATA")
	if s then
		silent_list2 = string.Explode("\r\n", s)
		for k,v in pairs(silent_list2) do
			if !silent_list[v] then silent_list[v] = true end
		end
	else
		local output = "weapon_example1\r\nweapon_example2"
		if !file.IsDir("stealth", "DATA") then
			file.CreateDir("stealth")
		end
		file.Write("stealth/silent.txt", output)
	end
	
	local s = file.Read("stealth/npc.txt", "DATA")
	if s then
		npc_list2 = string.Explode("\r\n", s)
		for k, v in pairs(npc_list2) do
			npc_list[v] = true
		end
		-- PrintTable(npc_list)
	else
		--npc_list = npc_default
		local output = string.Implode("\r\n", table.GetKeys(npc_list))
		if !file.IsDir("stealth", "DATA") then
			file.CreateDir("stealth")
		end
		file.Write("stealth/npc.txt", output)
	end
	
	local s = file.Read("stealth/suppressed.txt", "DATA")
	if s then
		suppressed_list2 = string.Explode("\r\n", s)
		for k,v in pairs(suppressed_list2) do
			if !suppressed_list[v] then suppressed_list[v] = true end
		end
	else
		local output = "weapon_example3\r\nweapon_example4"
		if !file.IsDir("stealth", "DATA") then
			file.CreateDir("stealth")
		end
		file.Write("stealth/suppressed.txt", output)
	end
	
	local s = file.Read("stealth/ignoresounds.txt", "DATA")
	if s then
		ignoresounds_list2 = string.Explode("\r\n", s)
		for k,v in pairs(ignoresounds_list2) do
			if !ignoresounds_list[v] then ignoresounds_list[v] = true end
		end
	else
		local output = string.Implode("\r\n", table.GetKeys(ignoresounds_list))
		if !file.IsDir("stealth", "DATA") then
			file.CreateDir("stealth")
		end
		file.Write("stealth/ignoresounds.txt", output)
	end
	
	print ("Stealth Mod files loaded")
end

concommand.Add("stealth_reloadsettings", function ()
	load_files()
end)

hook.Add("Initialize", "initializing_stealth", function()
	load_files()
end)

local function IsVisible(ply,npc)
	
	if ply.stealth_iscloaked then return false end
	
	/*for boneid = 0, ply:GetBoneCount() do -- Considering how many bones the average playermodel has, the other way is worth it.
		local tr = util.TraceLine{
			start = npc:EyePos(),
			endpos = ply:GetBonePosition(boneid),
			filter = npc,
			mask = MASK_BLOCKLOS_AND_NPCS
		}
		if tr.Entity == ply then return true end
	end*/
	
	
	for i = 0, ply:GetHitboxSetCount()-1 do -- Having to make two loops just to get the hitbox positions is stupid.
		for o = 0, ply:GetHitBoxCount(i)-1 do
			local tr = util.TraceLine{
				start = npc:EyePos(),
				endpos = ply:GetBonePosition(ply:GetHitBoxBone(o,i)),
				filter = npc,
				mask = MASK_BLOCKLOS_AND_NPCS
			}
			if tr.Entity == ply then return true end
		end
	end
	
	return false
	
end

local function IsEntVisible(ent,npc)
	local tr = util.TraceLine{
		start = npc:EyePos(),
		endpos = ent:GetPos(),
		filter = npc,
		mask = MASK_BLOCKLOS_AND_NPCS
	}
	
	return tr.Entity == ent
end

--Makes an NPC actually fully see and attack the player
local function alert(ply, npc, silent)
	--The npc.FRMIgnore thing is compatibility with FiLzO's revive mod; it stops dead NPCs from alerting on you.
	--print(GetConVarNumber("stealth_cheat_no_alert"))
	if !ply:Alive() or (not npc.stealth_MEnemies) or npc.FRMignore then return end
	if cvars.Bool("stealth_cheat_no_alert") then return end
	if IsValid(npc.stealth_MEnemies[ply:EntIndex()]) then return end
	npc:SetSchedule(0)
	table.insert(npc.stealth_MEnemies, ply:EntIndex(), ply)
	npc:AddEntityRelationship(ply, D_HT, 1)
	if silent and npc:GetNPCState() <= NPC_STATE_ALERT then
		npc:SetLastPosition(ply:GetPos())
		npc:SetEnemy(ply)
		npc:UpdateEnemyMemory( ply, ply:GetPos() ) 
		npc:SetSchedule( SCHED_CHASE_ENEMY  )
		npc:FoundEnemySound() --This probably doesn't work...
	end

	npc.stealth_investigating = 0
	npc.stealth_targetpos = nil
	npc.stealth_running = false
	npc:SetNWBool("stealth_alerted",true)
	
	-- Little delay, so it doesn't display anything if the enemy died intantly
	timer.Simple(0.1, function()
		if IsValid(npc) and IsValid(ply) and npc:Health() > 0 then
			
			-- Tell player that he alerted someone
			net.Start("NPCAlerted")
				net.WriteEntity(npc)
				net.WriteBool(silent)
			net.Send(ply)
			-- Broadcast alert effect
			net.Start("NPCEffect")
				net.WriteEntity(npc)
				net.WriteString("alert")
			net.Broadcast()
			
		end
	end)
	if !silent then
		timer.Simple(cvars.Number("stealth_backuptime"), function() 
			if IsValid(npc) and IsValid(ply) then
				for k, v in pairs(npctable) do
					if IsValid(v) and v != npc and npc:GetPos():DistToSqr(v:GetPos()) <= cvars.Number("stealth_backuprange")*cvars.Number("stealth_backuprange") then alert(ply, v, true)	end
				end
			end
		end) 
	end
end

function sendinvestigate(npc, pos, run)
	investigate(npc, pos, run, false)
end

local function investigate(npc, pos, run, nearest)
	-- If nearest is true, then only change target if its closer than the last one
	-- If its false, then always change target
	-- This prevents the NPC from changing target constantly if he see more than one player at the same time
	if !table.IsEmpty(npc.stealth_MEnemies) then return end
	if npc.invdelay > CurTime() then return end
	npc.invdelay = CurTime() + 3
	if npc.stealth_investigating == 0 then
		-- Little delay, so it doesn't display anything if the enemy died intantly
		timer.Simple(0.1, function()
			if IsValid(npc) and npc:Health() > 0 then
				-- Broadcast alert effect
				net.Start("NPCEffect")
					net.WriteEntity(npc)
					net.WriteString("caution")
				net.Broadcast()
			end
		end)
	end
	npc.stealth_investigating = 1
	--if nearest == false or (nearest == true and (npc.stealth_targetpos == nil or npc:GetPos():Distance(npc.stealth_targetpos) >= npc:GetPos():Distance(pos))) then
	npc.stealth_targetpos = pos
	npc:SetLastPosition(pos)
	if run == false and npc.stealth_running == false then
		npc:SetSchedule( SCHED_FORCED_GO )
	else
		npc:SetSchedule( SCHED_FORCED_GO_RUN )
		npc.stealth_running = true
	end
	--end
end

local function calm(ply, npc)
	-- Always calm him down, don't wait until he loses track of the player
	-- if ply==npc:GetEnemy() then return end
	for k, v in pairs(npc.stealth_MEnemies) do --for k = #npc.stealth_MEnemies, 1, -1 do
		--local v = npc.stealth_MEnemies[k]
		if v==ply then
			npc:AddEntityRelationship(ply, D_NU, 1)
			table.remove(npc.stealth_MEnemies,k)
			-- Make npc forget the player
			npc:SetTarget(npc)
			
			--Make it make a noise signifying that the search is off...
			npc:LostEnemySound()
			
			-- Tell player that an npc is not looking for him anymore
			net.Start("NPCCalmed")
				net.WriteEntity(npc)
			net.Send(ply)
			-- Broadcast alert effect
			net.Start("NPCEffect")
				net.WriteEntity(npc)
				net.WriteString("caution")
			net.Broadcast()
			return
		end
	end
	if table.IsEmpty(npc.stealth_MEnemies) then npc:SetNWBool("stealth_alerted",false) end
end

function GetNPCSchedule( npc )
	if ( !IsValid( npc ) ) then return end
	for s = 0, LAST_SHARED_SCHEDULE-1 do
		if ( npc:IsCurrentSchedule( s ) ) then return s end
	end
	return 0
end

local function checkLOS(ply, npc)
	-- Variables
	local minsight = cvars.Number("stealth_minsight")
	local maxsight = cvars.Number("stealth_maxsight")
	local movementbonus = cvars.Number("stealth_movebonus")
	local minhearing = cvars.Number("stealth_minhearing")
	local shotrange = cvars.Number("stealth_shotrange")
	local multiplier = cvars.Number("stealth_multiplier")
	if minsight < 0 then minsight = 0 end
	if maxsight < 0 then maxsight = 0 end
	if movementbonus < 0 then movementbonus = 0 end
	if minhearing < 0 then minhearing = 0 end
	if shotrange < 0 then shotrange = 0 end
	if multiplier < 0 then multiplier = 0 end
	--
	
	if not ply:Alive() then return end
	if not ply then return end
	if not ply.stealth_Luminocity then return end
	if not ply.stealth_shooting then ply.stealth_shooting = 0 end

	local isvisible = IsVisible(ply,npc)
		
	-- ConVar to number
	local alerttime = 0
	if cvars.Number("stealth_alerttime") >= 0 then alerttime = cvars.Number("stealth_alerttime") end
	--
	
	if not isvisible then 
		if not timer.Exists(npc:EntIndex().."Cooldown") then
			-- Use ConVar as timer duration
			timer.Create(npc:EntIndex().."Cooldown",alerttime,1,function() if IsValid(npc) then npc:SetTarget(npc) calm(ply, npc) end end)
			--
		end 
	elseif timer.Exists(npc:EntIndex().."Cooldown") then
		-- Use ConVar as timer duration
		timer.Adjust(npc:EntIndex().."Cooldown",alerttime,1, function() if IsValid(npc) then npc:SetTarget(npc) calm(ply, npc) end end)
		--
	end

	if ply.stealth_shooting != 0 then ply.stealth_Luminocity = math.Clamp(ply.stealth_Luminocity + ply.stealth_shooting*100,0,255) end
	--local eyesang = nil
	local yawdiff = math.abs(math.AngleDifference(npc:EyeAngles().y, (ply:GetPos()-npc:GetPos()):Angle().y))
	--local eyesobj = npc:LookupAttachment( "eyes" )
	--if eyesobj then eyesang = npc:GetAttachment( eyesobj ) end
	--if eyesang then yawdiff = math.abs(math.AngleDifference(eyesang["Ang"].y,(ply:GetPos()-npc:GetPos()):Angle().y))
	--else yawdiff = math.abs(math.AngleDifference(npc:GetAngles().y,(ply:GetPos()-npc:GetPos()):Angle().y)) end
	local lightbonus = ((maxsight - minsight) * (ply.stealth_Luminocity/255))
	local playerspeed = ply:GetVelocity():Length()
	local playerdist = ply:GetPos():DistToSqr(npc:GetPos())
	-- Sight and hearing are different variables
	local sightrange = ( minsight + lightbonus * (1+(movementbonus*(playerspeed/200))) ) * multiplier
	sightrange = sightrange*sightrange
	local hearrange = ( minhearing * (playerspeed/200) ) * multiplier
	--local shotrange = shotrange * multiplier
	
	-- If ai_disabled == 1 or ai_ignoreplayers == 1, ignore players
	if cvars.Bool("ai_disabled") or cvars.Bool("ai_ignoreplayers") or ply.notarget == true then
		sightrange = 0
		hearrange = 0
		ply.stealth_shooting = 0
	else
		-- Enemies can't see what's behind them or behind walls
		if yawdiff>60 or not isvisible then
			sightrange = 0
		end
				
		-- If player is walking or crouching, enemies can't hear him. They can't hear behind walls either
		if ply:GetVelocity():LengthSqr() < 12100 or not isvisible then
			hearrange = 0
		end
		
		-- If player is hiding under a cardboard box, and is not moving, ignore him
		local wep = ply:GetActiveWeapon()
		if IsValid(wep) then
			if wep:GetClass() == "weapon_cbox" and ply:Crouching() and ply:GetVelocity():LengthSqr() < 32 then
				sightrange = 0
				hearrange = 0
			end
		end
	end
	local shotrange = shotrange * ply.stealth_shooting * multiplier
	
	--if ply:Crouching() then sightrange = sightrange/2 end
	if not isvisible then ply.stealth_shooting = ply.stealth_shooting/2 end
	
	--[[ DEBUG
	print("-------------------------")
	print("SightRange: "..sightrange)
	print("HearRange: "..hearrange)
	print("Shooting: ".. ply.stealth_shooting*shotrange)
	print("Player distance: ".. playerdist)
	]]--
	
	if (sightrange*0.75)>playerdist then
		alert(ply, npc, false)
	elseif (sightrange>playerdist or hearrange*hearrange>playerdist) then
		investigate(npc, ply:GetPos(), false, true)
	elseif (shotrange*shotrange)>playerdist then
		investigate(npc, ply:GetPos(), true, false)
	end

	ply.stealth_shooting = 0
end

local function checkEntityLOS(ent, npc)
	-- Variables
	local minsight = cvars.Number("stealth_minsight")
	local maxsight = cvars.Number("stealth_maxsight")
	local multiplier = cvars.Number("stealth_multiplier")
	if minsight < 0 then minsight = 0 end
	if maxsight < 0 then maxsight = 0 end
	if multiplier < 0 then multiplier = 0 end
	--
	
	local isvisible = IsEntVisible(ent,npc)
	local eyesang = nil
	local yawdiff = 0
	local eyesobj = npc:LookupAttachment( "eyes" )
	if eyesobj then eyesang = npc:GetAttachment( eyesobj ) end
	if eyesang then yawdiff = math.abs(math.AngleDifference(eyesang["Ang"].y,(ent:GetPos()-npc:GetPos()):Angle().y))
	else yawdiff = math.abs(math.AngleDifference(npc:GetAngles().y,(ent:GetPos()-npc:GetPos()):Angle().y)) end
	local entdist = ent:GetPos():DistToSqr(npc:GetPos())
	local sightrange = maxsight * multiplier
	--
	
	if yawdiff>60 or not isvisible then
			return -1
	end
	
	if (sightrange*sightrange*0.75)>=entdist then
		--investigate(npc, ent:GetPos(), false, false)
		-- Return distance to target
		return entdist
	end
	return -1
end

function GetAllConditions ( npc )
	for i=0, 70 do
		if npc:HasCondition( i ) then print (npc:ConditionName( i ) ) end
	end
	print("-----------")
end

timer.Create("NPCStealthThink",.2,0,function() --Keep checking what npcs are alert to
	if !cvars.Bool("stealth_enabled") then return end
	-- Check Player LOS
	for k, v in pairs(npctable) do --for k = #npctable, 1, -1 do
		--local v = npctable[k]
		
		if IsValid(v) then
			if v.stealth_initpos == nil then v.stealth_initpos = v:GetPos() end
			if v.stealth_initangles == nil then v.stealth_initangles = v:GetAngles() end
			-- GetAllConditions ( v )
			for o,p in ipairs(player.GetAll()) do
				checkLOS(p,v)
			end
			-- Check corpse LOS			
			local nearestcorpse = nil
			local nearestdistance = -1
			for o, p in pairs(corpsetable) do --for o = #corpsetable, 1, -1 do
				--local p = corpsetable[o]
				if IsValid(p) then
					if !IsValid(v.stealth_seencorpses[p:EntIndex()]) then
						local dist = checkEntityLOS(p,v)
						if dist != -1 then
							table.insert(v.stealth_seencorpses,p:EntIndex(), p)
							if nearestdistance == -1 or dist < nearestdistance*nearestdistance then
								nearestdistance = dist
								nearestcorpse = p
							end
						end
					end
				else
					table.remove(corpsetable,o)
				end
			end
			if IsValid(nearestcorpse) then investigate(v, nearestcorpse:GetPos(), true, false) end
			
			for o, p in pairs(v.stealth_MEnemies) do --for o = #v.stealth_MEnemies, 1, -1 do
				--local p = v.stealth_MEnemies[o]
				if !IsValid(p) then table.remove(v.stealth_MEnemies,o) end
			end
			if table.IsEmpty(v.stealth_MEnemies) then v:SetNWBool("stealth_alerted",false) end
		else
			table.remove(npctable,k)
			-- Send Clients the signal to remove NPC
			net.Start("RemoveNPCfromTable")
				net.WriteEntity(v)
			net.Broadcast()
		end
	end
	
	-- Check if enemies have arrived their investigation target point
	if !table.IsEmpty(npctable) then
		SetGlobalBool("BadNPCOnMap", true)
		for k, v in pairs(npctable) do
			if v.stealth_investigating == 1 and v.stealth_targetpos != nil and v:GetPos():DistToSqr(v.stealth_targetpos)<10000 then
				v.stealth_targetpos = nil
				v.stealth_running = false
				v:SetSchedule( 1 )
				timer.Simple(1, function()
					if IsValid(v) then
						v:SetSchedule( SCHED_ALERT_SCAN  )
					end
				end)
				timer.Simple(4, function()
					if IsValid(v) then
						if v.stealth_initpos!=nil and v.pr_prcontroller then
							v.stealth_investigating = 0
						else
							v.stealth_investigating = 2
							v:SetLastPosition(v.stealth_initpos)
							v:SetSchedule( SCHED_FORCED_GO_RUN )
						end
					end
				end)
				-- Broadcast alert effect
				--umsg.Start( "NPCEffect" )
				--	umsg.Entity( v )
				--	umsg.String( "caution" )
				--umsg.End();	
			end
			if v.stealth_investigating == 2 and v.stealth_initangles != nil and v:GetPos():DistToSqr(v.stealth_initpos)<2500 then
				v.stealth_running = false
				v:SetSchedule( 1 )
				v.stealth_investigating = 0
				v.stealth_targetpos = nil
				timer.Simple(1, function()
					if IsValid(v) then
						v:SetAngles(v.stealth_initangles)
					end
				end)
			end
		end
	else
		SetGlobalBool("BadNPCOnMap", false)
	end
	-- Delete corpses
	if table.Count(corpsetable) > cvars.Number("stealth_maxcorpses") then
		for k = (table.Count(corpsetable) - cvars.Number("stealth_maxcorpses")), 1, -1 do
			corpsetable[table.maxn(corpsetable)]:Remove() 
			table.remove(corpsetable,table.maxn(corpsetable))
		end
	end
	
	-- DEBUG STUFF
	/*for k,v in ipairs(ents.FindByClass("npc_*")) do
		if v:GetNWBool("sm_sleepingNPC") then print(timer.Exists("wake_time_"..v:EntIndex())) end
	end*/
	
end)

function CorpseCreate(ent,ragdoll)
	if cvars.Bool("stealth_enabled") and IsValid(npctable[ent:EntIndex()]) and (!TFA_BGO) then
		table.insert(corpsetable,ragdoll:EntIndex(), ragdoll)
	end
end

hook.Add("CreateEntityRagdoll","AddCorpseToTable",CorpseCreate)

hook.Add("PlayerDeath","StopChase",function(ply,item,attacker)
	for o, p in pairs(npctable) do --for o = #npctable, 1, -1 do
		--local p = npctable[o]
		if !IsValid(p.stealth_MEnemies) then continue end
		for k, v in pairs(p.stealth_MEnemies) do --for k = #p.stealth_MEnemies, 1, -1 do
			--local v = p.stealth_MEnemies[k]
			if v==ply then
				p:AddEntityRelationship(ply, D_NU, 1)
				table.remove(p.stealth_MEnemies,k)
				-- Make npc forget the player
				--p:SetTarget(p)
				
				-- Tell player that an npc is not looking for him anymore
				net.Start("NPCCalmed")
					net.WriteEntity(p)
				net.Send(ply)
			end
		end
		if table.IsEmpty(p.stealth_MEnemies) then p:SetNWBool("stealth_alerted",false) end
	end
end)

hook.Add("PlayerDeath","KillSleepingPlayer",function(ply,item,attacker)
	if IsValid(ply:GetNWEntity("sm_SpecRagdoll")) then
		ply:GetNWEntity("sm_SpecRagdoll"):Remove()
		ply:SetNWEntity("sm_SpecRagdoll", nil)
		ply:Freeze(false)
	end
end)

hook.Add("PlayerDisconnected","DiscSleepingPlayer",function(ply)
	if IsValid(ply:GetNWEntity("sm_SpecRagdoll")) then
		ply:GetNWEntity("sm_SpecRagdoll"):Remove()
		ply:SetNWEntity("sm_SpecRagdoll", nil)
	end
end)

hook.Add("OnNPCKilled", "ZDeathHappen", function(npc, attacker, inflictor )
	-- Blood and Gore Overhaul compatibility
	if TFA_BGO and cvars.Bool("sv_bgo_enabled") and IsValid(npctable[npc:EntIndex()]) then	
		local bgocorpses = ents.FindByClass("bgo_ragdoll")
		--print(bgocorpses[#bgocorpses])
		table.insert(corpsetable, bgocorpses[#bgocorpses].targent:EntIndex(), bgocorpses[#bgocorpses].targent)
	end
	local corpses = ents.FindByClass("prop_ragdoll")
	-- Gibmod Compatibility
	if corpses[#corpses] and corpses[#corpses].GibMod_DeathRag and corpses[#corpses].GibMod_DeathRag then
		table.insert(corpsetable, corpses[#corpses]:EntIndex(), corpses[#corpses])
	end
end)

hook.Add("AddCorpse", "AddToCorpseList", function(ent) 
	table.insert(corpsetable,ent:EntIndex(), ent)
end)

hook.Add("EntityTakeDamage","AlertAttackedNpc",function(attacked,damage)
	local attacker = nil
	if damage:GetAttacker() then attacker = damage:GetAttacker() end
	if IsValid(attacked) and attacked:IsNPC() and IsValid(npctable[attacked:EntIndex()]) and IsValid(attacker) and attacker:IsPlayer() and !IsValid(attacked.stealth_MEnemies[attacker:EntIndex()])then
		if cvars.Bool("ai_disabled") or cvars.Bool("ai_ignoreplayers") or attacker.notarget then return
		elseif cvars.Bool("stealth_alertondamage") then alert(attacker, attacked, false)
		else investigate(attacked,attacker:GetPos(),true,false) end
	end
end)

hook.Add("EntityTakeDamage","StunnedTakesDamage",function(npc,dmginfo)
	if npc:GetNWBool("sm_sleepingNPC") and dmginfo:GetDamageType() != DMG_CRUSH then
		npc:SetNWInt("sm_NPChealth", npc:GetNWInt("sm_NPChealth") - dmginfo:GetDamage())
		
		local effectdata = EffectData()
		effectdata:SetStart( dmginfo:GetDamagePosition( ) ) 	
		effectdata:SetAngles( npc:GetAngles() )
		effectdata:SetOrigin( dmginfo:GetDamagePosition( ) )
		effectdata:SetScale( 1 )
		util.Effect( "BloodImpact", effectdata )
		
		if npc:GetNWInt("sm_NPChealth") <= 0 then
			local ply = npc:GetNWEntity("sm_SpecPlayer")
			if IsValid(ply) then
				ply:UnSpectate()
				ply:Spawn()
				ply:SetPos(npc:WorldSpaceCenter())
				dmginfo:SetDamage(2147483647)
				ply:TakeDamageInfo(dmginfo)
				npc:Remove()
			else
				hook.Run( "AddCorpse", npc)
				npc:SetNWBool("sm_sleepingNPC", false)
			end
		end
	end
end)

hook.Add("KeyPress","AlertShootingNPC",function(ply,key)
	if IsValid(ply) and key==IN_ATTACK and !table.IsEmpty(npctable) and cvars.Bool("stealth_enabled") then
		local wep = ply:GetActiveWeapon()
		if IsValid(wep) then
		
			if silent_list[wep:GetClass()] or wep:Clip1()<=0 or (CurTime() < wep:GetNextPrimaryFire()) then return end
			
			if (wep.dt and wep.dt.Suppressed) or ( type(wep.GetSilenced) == "function" and wep:GetSilenced()) or suppressed_list[wep:GetClass()] then
				ply.stealth_shooting = cvars.Number("stealth_suppmultiplier")
			else
				ply.stealth_shooting = 1
			end
			
			for k, v in pairs(npctable) do --for k = #npctable, 1, -1 do
				--local v = npctable[k]
				if IsValid(v) then
					checkLOS(ply,v)
				else
					table.remove(npctable,k)
					-- Send Clients the signal to remove NPC
					net.Start("RemoveNPCfromTable")
						net.WriteEntity(ent)
					net.Broadcast()
				end
			end
		end
	end
end)

hook.Add( "PlayerButtonDown", "PlayerWakeUpStruggle", function( ply, button )
	local specrag = ply:GetNWEntity("sm_SpecRagdoll")
	if IsValid(specrag) and button == KEY_E then
		timer.Adjust("wake_time_"..specrag:EntIndex(), timer.TimeLeft("wake_time_"..specrag:EntIndex()) - 0.2)
	end
end)


local potentialenemies = {}
hook.Add("OnEntityCreated","GiveHearing",function(ent) --Startnpcs
	if !cvars.Bool("stealth_enabled") then return end
	local playerlist = player.GetAll()
	-- Only add npcs if they are in the list (the ent.FRMignore bit is compatibility with FiLzO's reviving addon; don't want dead NPCs seeing you)
	if ent:IsNPC() and npc_list[ent:GetClass()] and not ent.FRMignore then
		if playerlist[1] then
			if ent:Disposition(playerlist[1])==1 or cvars.Number("stealth_override") != 0 then
				table.insert(npctable, ent:EntIndex(), ent)
				ent.stealth_MEnemies = {}
				if cvars.Bool("stealth_keepcorpses") and !TFA_BGO then ent:SetShouldServerRagdoll( true ) end
				for k, v in ipairs(playerlist) do
					ent:AddEntityRelationship(v, D_NU, 1)
				end
				-- Send Clientes the new NPC
				net.Start("AddNPCtoTable")
					net.WriteEntity(ent)
				net.Broadcast()
			end
		else
			table.insert(potentialenemies, ent)
		end
		-- Variables initialization
		ent.stealth_investigating = 0
		ent.invdelay = CurTime()
		ent.stealth_running = false
		ent.stealth_targetpos = nil
		ent.stealth_initpos = nil
		ent.stealth_initangles = nil
		ent.stealth_seencorpses = {}
		ent.stealth_sound_memory = {}
		ent:SetNWBool("stealth_stealthNPC",true)
		ent:SetNWBool("stealth_alerted",false)
		--
	elseif ent:IsPlayer() then
		for k, v in pairs(potentialenemies) do
			if IsValid(v) and v:Disposition(ent)==1 then
				table.insert(npctable,v:EntIndex(), v)
				v.stealth_MEnemies = {}
				-- Send Clients the new NPC
				net.Start("AddNPCtoTable")
					net.WriteEntity(ent)
				net.Broadcast()
			end
		end
		potentialenemies = {}
		for k, v in pairs(npctable) do
			v:AddEntityRelationship(ent, D_NU, 1)
		end
	end
end)

--Make NPCs able to hear and investigate all sounds
--See https://wiki.garrysmod.com/page/Structures/EmitSoundInfo for info on the table that gets passed in 
hook.Add("EntityEmitSound", "StealthGeneralHearing", function (soundinfo)
	hearing_range = cvars.Number("stealth_general_hearing_range") --The absolute maximum distance a sound can be heard; make this very big!
	
	--Ragdolls make lots of really quiet noises, so let's just not even bother with sounds they make.
	if soundinfo.Entity:IsValid() and soundinfo.Entity:GetClass() == "prop_ragdoll" then return end
	if ignoresounds_list[soundinfo.OriginalSoundName] then return end
	--if soundinfo.Entity:IsValid() and soundinfo.Entity:IsWeapon() then return end
	
	--This is a multiplier from 0 to 1; NPCs only "hear" sounds in the hearing range times this factor.
	--I think the SoundLevel is supposed to represent inherent loudness - one sound might be louder than another even though both are)
	--played at 100% - so this factor accounts for that and shortens the hearing range of inherently quieter sounds.
	--This probably shouldn't be linear; see http://www.engineeringtoolbox.com/inverse-square-law-d_890.html
	--In fact I ended up cubing it, so as to make the difference between quiet and loud sounds bigger.
	attenuation_factor = (soundinfo.SoundLevel / 511)
	attenuation_factor = attenuation_factor * attenuation_factor * attenuation_factor
	--The actual playback volume set when asked Source was asked to play the sound; this is already in a convenient 0-1 percentage format
	playback_volume_factor = soundinfo.Volume
	
	--The actual distance from the sound's origin at which any NPC should hear the sound
	effective_hearing_range = hearing_range * attenuation_factor * playback_volume_factor
	
	--debug_print("Effective audibility range of ".. soundinfo.SoundName .." emitted by ".. soundinfo.Entity:GetClass() .." is ".. effective_hearing_range .. "...")
	
	--Sometimes the sound itself has an origin position...
	source_pos = soundinfo.Pos
	if source_pos == nil then
		--But sometimes it doesn't, so let's use the origin of the entity that made the sound...
		if soundinfo.Entity:IsValid() then source_pos = soundinfo.Entity:GetPos() end
		--Unless that doesn't exist either; in which case there's no point in hearing it.
		if source_pos == nil then return end
	end
	
	--If any NPC is closer to the sound's origin than the maximum distance at which it can be heard, that NPC must've heard it!
	for k, v in pairs(npctable) do
		if v:IsValid() then 
			if v:GetPos():DistToSqr(source_pos) < effective_hearing_range*effective_hearing_range then
				--so send it to check it out.
				--debug_print(v:GetClass() .. " heard "..soundinfo.SoundName.." and is investigating.")
				investigate(v, source_pos, false, true)
			else
				--debug_print(v:GetClass() .. " didn't hear "..soundinfo.SoundName..".")
			end
		end
	end
end)

-- Dragging stuff
local function dropbody(ply)
	local wep = ply:GetActiveWeapon()
	if (IsValid(wep)) then
		wep:SetNextPrimaryFire(CurTime())
		wep:SetNextSecondaryFire(CurTime())
	end
	ply.holdEnt = NULL
	--if (!ply.visibleGun) then
		ply:DrawViewModel(true)
		ply:DrawWorldModel(true)
	--end
	ply.hasDropped = true
end

local function dragbody(ply)
	ply.hasDropped = false;
	if (IsValid(ply.holdEnt)) then
		dropbody(ply)
		return
	else
		local tr = {}
		tr.start = ply:GetShootPos()
		tr.endpos = tr.start + ply:GetAimVector() * 70
		tr.filter = ply
		tr = util.TraceLine(tr)
		if (!IsValid(ply:GetVehicle()) && IsValid(tr.Entity) && (IsValid(corpsetable[tr.Entity:EntIndex()]) or tr.Entity:GetNWBool("sm_sleepingNPC"))) then
			ply.holdEnt = tr.Entity
			ply.holdPhys = tr.PhysicsBone
			if (IsValid(ply:GetActiveWeapon())) then
				ply.visibleGun = !ply:GetActiveWeapon():IsWeaponVisible()
				ply.weapon = ply:GetActiveWeapon():GetClass()
			end											
		end
	end
end

hook.Add("SetupMove", "startdrag", function(ply, mvd, cmd)
	if mvd:KeyDown(IN_USE) then
		dragbody(ply)
	end
end)

local function dragThink()
	for _, v in ipairs(player.GetAll()) do
		if (IsValid(v.holdEnt)) then
			local wep = v:GetActiveWeapon()
			if (IsValid(wep)) then
				wep:SetNextPrimaryFire(CurTime()+10)
				wep:SetNextSecondaryFire(CurTime()+10)
				v:DrawViewModel(false)
				v:DrawWorldModel(false)
				
				if (wep:GetClass() != v.weapon) then
					v:SelectWeapon(v.weapon)
				end
			end
			
			if v:GetGroundEntity() == v.holdEnt or v:InVehicle() then
				dropbody(v)
				return
			end
			
			local physobj = v.holdEnt:GetPhysicsObjectNum(v.holdPhys)
			local pos1 = v:GetShootPos() + v:GetAimVector() * 60
			local pos2 = physobj:GetPos()
						
			if (pos1:DistToSqr(pos2) > 10000) then
				dropbody(v)
				return
			else	
				v:SetLocalVelocity(v:GetVelocity()/2)
				local pos3=-(pos2 - pos1)
				pos3.z=0
				physobj:SetVelocity(pos3*8)
			end
			
		elseif (!v.hasDropped) then
			dropbody(v)
		end
	end
end

hook.Add("Think","dragthink",dragThink)

--Usually HL2 maps only have a couple NPCs in the game at a time; it spawns the Combine in a room right before you actually enter that room.
--That's great for an action FPS but having people suddenly appear from nowhere is less great for Tactical Espionage Action, so unless the
--user has turned it off we force all the spawners in the level to spawn their NPC once the game begins.
local function force_activate_enemy_spawners()
	for k, v in ipairs(ents.GetAll()) do
		class = v:GetClass()
		--PrintTable(v:GetKeyValues())
		--If it's an npc_maker and the NPC class it spawns is one we care about...
		if class == "npc_maker" and npc_list[v:GetKeyValues().NPCType] then
			v:Fire("Spawn")
			debug_print("Spawning a "..v:GetKeyValues().NPCType.." from an npc_maker.")
		elseif class == "npc_template_maker" then
			template_parentname = v:GetKeyValues().TemplateName
			debug_print("npc_template_maker's TemplateName is ".. template_parentname)
			--There doesn't seem to be any way to tell what a npc_template_maker is going to spawn, so we just trip them all
			v:Fire("Spawn")
		end
	end
end

hook.Add("InitPostEntity", "StealthForceEnemySpawns", function() 
	if cvars.Bool("force_enemy_spawns_on_level_load") then
		force_activate_enemy_spawners()
	end
end)

concommand.Add("force_activate_enemy_spawners", force_activate_enemy_spawners, nil, "Force all spawners on the level that spawn stealth-enabled NPCs to spawn.")

local function set_alpha(ent, alpha)
	local color = ent:GetColor()
	color.a = alpha
	ent:SetColor(color)
end

local function toggle_cloak(ply)
	--Don't allow turning the cloak on if cloaking is disabled - but still allow turning it off.
	if (not IsValid(ply)) then return end
	print(ply.stealth_iscloaked)
	
	if (ply.stealth_iscloaked == true) then
		ply:SetMaterial("")
		for k, v in ipairs(ply:GetWeapons()) do
			v:SetMaterial("")
		end
		set_alpha(ply, 255)
		ply.stealth_iscloaked = nil
		--ply:SetNoTarget(false)
		timer.Remove("stealth_cloaking_update_for_"..ply:GetName() )
		--chat.addText("You are no longer cloaked.")
	else
		if !ply:Armor() then return end
		if !cvars.Bool("stealth_cloak_allowed") then return end
		print(cvars.Bool("stealth_cloak_allowed"))
		--Defaults to "/models/props_c17/fisheyelens"; the "hunter" material sucks 'cos it moves, too easy to see.
		local cloakmat = cvars.String("stealth_cloak_material")
		ply:SetMaterial(cloakmat)
		ply.stealth_iscloaked = true
		--ply:SetNoTarget(true) --The notarget stuff works is super-cool, but it's too OP.
		set_alpha(ply, 127) --I don't think this actually works.
		
		for k, v in ipairs(ply:GetWeapons()) do
			v:SetMaterial(cloakmat)
		end
		
		timer.Create("stealth_cloaking_update_for_"..ply:GetName() ,0.33, 0, function()
			if ply:Armor() <= 0 then 
				toggle_cloak(ply)
				--chat.addText("You ran out of power and uncloaked!")
			else
				ply:SetArmor(ply:Armor() - 1)
				--We already set all the player's weapons to be cloaked when he toggled,
				--this is for if he picks up / spawns one while cloaked
				ply:GetActiveWeapon():SetMaterial(cloakmat)
			end
		end) 
	end
	
end

concommand.Add("stealth_toggle_cloak", toggle_cloak, nil, "Engage or disengage cloaking, which makes you hard to see to other players and Stealth-enabled NPCs won't see you while you're cloaked. Being cloaked consumes suit energy (the 'armor' counter) and doesn't work once you run out.")

hook.Add( "MakeNoiseDistraction", "MakeNoise", function( pos, run )
	if !table.IsEmpty(npctable) and cvars.Bool("stealth_enabled") then
		local soundrange = cvars.Number("stealth_shotrange") * cvars.Number("stealth_multiplier")
		for k, v in pairs(npctable) do--for k = #npctable, 1, -1 do
			--local v = npctable[k]
			if IsValid(v) then
				local distance = pos:DistToSqr(v:GetPos())
				local tr = util.TraceLine{
					start = v:EyePos(),
					endpos = pos,
					filter = {v},
					mask = MASK_VISIBLE_AND_NPCS
				}
				if (tr.Hit and soundrange*soundrange/2 > distance) or (!tr.Hit and soundrange*soundrange > distance) then
					investigate(v, pos, run, false)
				end
			else
				table.remove(npctable,k)
				-- Send Clients the signal to remove NPC
				net.Start("RemoveNPCfromTable")
					net.WriteEntity(ent)
				net.Broadcast()
			end
		end
	end
end )

hook.Add( "TurnOnSensor", "AddSensor", function( ent )
	if IsValid(ent) and IsValid(ent:GetOwner()) then
		net.Start("AddSensor")
			net.WriteEntity(ent)
		net.Send(ent:GetOwner())
	end
end )

net.Receive("stealth_clientinitialized", function(len,ply)
	if ply:IsValid() and ply:IsPlayer() then
		for k,v in pairs(npctable) do
			if IsValid(v) then
				net.Start("AddNPCtoTable")
					net.WriteEntity(ent)
				net.Broadcast()
			end
		end
	end
end)

concommand.Add("p_sendluminocity",function(ply,com,arg)
	if arg[1] then
		ply.stealth_Luminocity = tonumber(arg[1])
	end
end)
