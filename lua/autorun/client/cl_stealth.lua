include( "sh_stealth.lua" )

local lastsend = 0
local luminocity = 0
local alertdanger = 0
local dangernpc = nil
local isnpc = false
local hunters = {}
local npctable = {}
local sensorlist = {}
local insight = 0
local alertmusic = nil
local playingmusic = 0
local fadingmusic = 0
local currenteffect = nil
local sounddelay = CurTime()

-- local dlight = DynamicLight( LocalPlayer():EntIndex() ) // Darkness vision light
-- local darkvisionbrightness = 0 // Used to fade in/out the darkness vision light.

stealthmod = {enablehud = 1, luminocity = 0, alertdanger = 0}

CreateClientConVar( "stealth_drawbar", 1) 
CreateClientConVar( "stealth_enablefx", 1) 
CreateClientConVar( "stealth_enablesound", 1) 
CreateClientConVar( "stealth_enablemusic", 1) 
-- CreateClientConVar( "stealth_enabledarkvision", 1)
CreateClientConVar( "stealth_hudx", 29) 
CreateClientConVar( "stealth_hudy", 220) 

hook.Add( "InitPostEntity", "StealthClientsideInitialization", function()
	alertmusic = CreateSound( LocalPlayer() , "#stealth/alertmusic.wav") 
	util.PrecacheSound("stealth/alert.wav")
	util.PrecacheSound("stealth/alertmusic.wav")
	net.Start("stealth_clientinitialized")
	net.SendToServer()
end )

local function AddNPCtoTable()
	local npc = net.ReadEntity()
	if IsValid( npc ) and !IsValid(npctable[npc:EntIndex()]) then
		table.insert(npctable, npc:EntIndex(), npc)
	end
end

net.Receive( "AddNPCtoTable", AddNPCtoTable )

local function RemoveNPCfromTable()
	local npc = net.ReadEntity()
	if IsValid( npc ) and IsValid(npctable[npc:EntIndex()]) then
		--table.RemoveByValue(npctable, npc)
		table.remove(npctable, npc:EntIndex())
	end
end

net.Receive( "RemoveNPCfromTable", RemoveNPCfromTable )

cvars.AddChangeCallback( "stealth_enablemusic", function( convar_name, value_old, value_new )
	if (tonumber(value_new) == 0 and alertmusic:IsPlaying()) then alertmusic:Stop()
	elseif (tonumber(value_new)) != 0 and !alertmusic:IsPlaying() and playingmusic == 1 then alertmusic:Play() end
end)

function draw.Circle( x, y, radius, seg )
	local cir = {}

	table.insert( cir, { x = x, y = y, u = 0.5, v = 0.5 } )
	for i = 0, seg do
		local a = math.rad( ( i / seg ) * -360 )
		table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
	end

	local a = math.rad( 0 ) -- This is need for non absolute segment counts
	table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )

	surface.DrawPoly( cir )
end

function AddSensor()
	local ent = net.ReadEntity()
	if IsValid( ent ) and !IsValid(sensorlist[ent:EntIndex()]) then
		table.insert(sensorlist, ent:EntIndex(), ent)
	end
end

net.Receive( "AddSensor", AddSensor )

--[[
I use an extra table to avoid calculating the NPCs under
sensor range in the PostDrawOpaqueRenderables event.
That should increase performance.
]]--
timer.Create("CheckNearSensor",.5,0,function() --Keep checking what alerted npcs are visible
	local range = cvars.Number("stealth_sensorrange")
	if !table.IsEmpty(sensorlist) then
		for k, v in ipairs(ents.FindByClass("npc_*")) do
			v.sm_NPCinsensor = -1
		end
		for k, v in pairs(sensorlist) do
			if !IsValid(v) then 
				table.remove(sensorlist, k)
				continue
			end
			for o, p in ipairs(ents.FindInSphere(v:GetPos(), range)) do
				if p:IsNPC() or p:IsNextBot() then
					p.sm_NPCinsensor = p:GetPos():DistToSqr(v:GetPos())
				end
			end
		end
		
	end
	-- Here I copy a networked variable to a client global one. That should
	-- reduce network transit.
	for k,v in ipairs( ents.FindByClass("prop_ragdoll") ) do
		v.sm_sleeping = v:GetNWBool("sm_sleepingNPC")
		v.sm_sleepinit = v:GetNWFloat("sm_sleepinit")
		v.sm_sleeptime = v:GetNWFloat("sm_sleeptime")
		v.sm_sleephealth = v:GetNWInt("sm_NPChealth")
	end
end)

hook.Add("PostDrawOpaqueRenderables","SensorWallhack",function()
	if table.IsEmpty(sensorlist) then return end
	local range = cvars.Number("stealth_sensorrange") * cvars.Number("stealth_sensorrange")
	--local allents = ents.FindByClass("npc_*")
	for k, v in ipairs(ents.FindByClass("npc_*")) do--for k = #allents, 1, -1 do
		--local v = allents[k]
		render.ClearStencil()
		if IsValid(v) and v:Health() > 0 and v.sm_NPCinsensor and v.sm_NPCinsensor != -1 then
			cam.Start3D()
				render.SetStencilEnable( true )
					cam.IgnoreZ( true )

						render.SetStencilWriteMask( 1 )
						render.SetStencilTestMask( 1 )
						render.SetStencilReferenceValue( 1 )

						render.SetStencilCompareFunction( STENCIL_ALWAYS )
						render.SetStencilPassOperation( STENCIL_REPLACE )
						render.SetStencilFailOperation( STENCIL_KEEP )
						render.SetStencilZFailOperation( STENCIL_KEEP )
						
						render.SetBlend(0)
						v:DrawModel()
						render.SetBlend(1)

						render.SetStencilCompareFunction( STENCIL_EQUAL )
						render.SetStencilPassOperation( STENCIL_KEEP )
							cam.Start2D()
								surface.SetAlphaMultiplier( math.Clamp(range - v.sm_NPCinsensor,0,range)/range )
								surface.SetDrawColor( Color( 255, 0, 0 ) )
								surface.DrawRect( 0, 0, ScrW(), ScrH() )
								surface.SetAlphaMultiplier( 1 ) 
							cam.End2D()
					cam.IgnoreZ( false )
					v:DrawModel()
				render.SetStencilEnable( false )
			cam.End3D()
		end
	end
end)

-- Drawing stars over heads. Credits to Olivia for the formula.
hook.Add( "PostDrawOpaqueRenderables", "SleepStars", function()
	for k,v in ipairs( ents.FindByClass("prop_ragdoll") ) do
		if (v.sm_sleeping and v.sm_sleepinit and v.sm_sleeptime and v.sm_sleephealth > 0) then
			local attpoint = v:GetAttachment(v:LookupAttachment("eyes"))
			
			if ( attpoint ) then
				local stars = math.ceil((1-((CurTime() - v.sm_sleepinit)/v.sm_sleeptime)) * 5)
				
				for i = 1, stars do
					local time = CurTime() * 3 + ( math.pi * 2 / stars * i )
					local offset = Vector( math.sin( time ) * 5, math.cos( time ) * 5, 15 )
					
					render.SetMaterial( Material( "stealth/star.png", "noclamp smooth" ) )
					render.DrawSprite( attpoint.Pos + offset, 3, 3, white )
				end
			end
		end
	end
end)

hook.Add("HUDPaint","DrawSplinterCellThing",function()
	if cvars.Number("stealth_drawbar")==0 or !cvars.Bool("cl_drawhud") or stealthmod.enablehud == 0 then return end
	local hudxpos = cvars.Number("stealth_hudx")
	local hudypos = ScrH()-cvars.Number("stealth_hudy")
	local down = ScrH()-200
	local width = 100
	local section = width/6
	
	-- HUD BASE
	if cvars.Number("stealth_drawbar")==1 then
		draw.RoundedBox(6,hudxpos,hudypos,200,100,Color(50,50,50,100))
		
		-- BAR 1 Text
		surface.SetFont( "Default" )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( hudxpos+10, hudypos+7 )
		surface.DrawText( "Visibility:" )
		
		-- BAR 2 Text
		surface.SetFont( "Default" )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( hudxpos+10, hudypos+53 )
		surface.DrawText( "Danger:" )
		
		-- CIRCLE
		surface.SetDrawColor( 0, 0, 0, 200 )
		draw.NoTexture()
		
		-- Outer Circle
		if !IsValid(dangernpc) then
			surface.SetDrawColor(0,0,0,255)
		else
			surface.SetDrawColor(255,255,255,255)
			-- Arrow
			local angle = math.rad(math.AngleDifference(LocalPlayer():GetAngles().y, (LocalPlayer():GetPos()-dangernpc:GetPos()):Angle().y)) + (math.pi/2)
			local radius = 25
			local centerx = hudxpos+160
			local centery = hudypos+50
			
			local triangle = {
				{ x = radius*math.cos(angle) + centerx, y = radius*math.sin(angle) + centery },
				{ x = (radius-12)*math.cos(angle-5.5) + centerx, y = (radius-12)*math.sin(angle-5.5) + centery },
				{ x = (radius-12)*math.cos(angle+5.5) + centerx, y = (radius-12)*math.sin(angle+5.5) + centery }
			}
			surface.DrawPoly( triangle )
		end
		
		draw.Circle( hudxpos+160, hudypos+50, 14, 50 )
	
	else
		surface.SetDrawColor(0,0,0,255)
		if cvars.Number("stealth_drawbar")==2 then draw.Circle( hudxpos+160, hudypos+50, 12, 50 )
		elseif cvars.Number("stealth_drawbar")==3 then draw.Circle( hudxpos+160, hudypos+35, 12, 50 )
		elseif cvars.Number("stealth_drawbar")==4 then draw.Circle( hudxpos+160, hudypos+80, 12, 50 ) end
	end
	
	-- Inner circle
	if table.IsEmpty(hunters) then
		surface.SetDrawColor(0,255,0,255)
	else
		if insight == 1 then surface.SetDrawColor(255,0,0,255) else surface.SetDrawColor(math.abs(math.sin(CurTime()*4))*255,math.abs(math.sin(CurTime()*4))*255,0,255) end
	end
	
	if cvars.Number("stealth_drawbar")==1 or cvars.Number("stealth_drawbar")==2 then draw.Circle( hudxpos+160, hudypos+50, 8, 50 )
	elseif cvars.Number("stealth_drawbar")==3 then draw.Circle( hudxpos+160, hudypos+35, 8, 50 )
	elseif cvars.Number("stealth_drawbar")==4 then draw.Circle( hudxpos+160, hudypos+80, 8, 50 ) end
	
	--surface.DrawRect(35,down+5,10,10)
	
	
	if cvars.Number("stealth_drawbar") >= 1 and cvars.Number("stealth_drawbar") <= 3 then
		-- Base
		local bar1xpos = hudxpos+10
		local bar1ypos = hudypos+25
		surface.SetDrawColor(0,0,0,255)
		surface.DrawRect(bar1xpos,bar1ypos,115,20)

		-- Blocks
		surface.SetDrawColor(51,51,51,255)
		surface.DrawRect(bar1xpos+4+section,bar1ypos+2,section-4,16)
		surface.SetDrawColor(102,102,102,255)
		surface.DrawRect(bar1xpos+6+section*2,bar1ypos+2,section-4,16)
		surface.SetDrawColor(153,153,153,255)
		surface.DrawRect(bar1xpos+8+section*3,bar1ypos+2,section-4,16)
		surface.SetDrawColor(204,204,204,255)
		surface.DrawRect(bar1xpos+10+section*4,bar1ypos+2,section-4,16)
		surface.SetDrawColor(255,255,255,255)
		surface.DrawRect(bar1xpos+12+section*5,bar1ypos+2,section-4,16)
		
		-- Slider
		local bar1 = luminocity*0.375
		draw.RoundedBox(4,bar1xpos+3+bar1,bar1ypos-2,10,24,Color(255,255,255,255))
	end
	
	if cvars.Number("stealth_drawbar") == 1 or cvars.Number("stealth_drawbar") == 2 or cvars.Number("stealth_drawbar") == 4 then
		-- Base
		local bar2xpos = hudxpos+10
		local bar2ypos = hudypos+70
		surface.SetDrawColor(0,0,0,255)
		surface.DrawRect(bar2xpos,bar2ypos,115,20)

		-- Blocks
		surface.SetDrawColor(0,100,0,255)
		surface.DrawRect(bar2xpos+4+section,bar2ypos+2,section-4,16)
		surface.SetDrawColor(0,150,0,255)
		surface.DrawRect(bar2xpos+6+section*2,bar2ypos+2,section-4,16)
		surface.SetDrawColor(0,200,0,255)
		surface.DrawRect(bar2xpos+8+section*3,bar2ypos+2,section-4,16)
		surface.SetDrawColor(255,255,0,255)
		surface.DrawRect(bar2xpos+10+section*4,bar2ypos+2,section-4,16)
		surface.SetDrawColor(255,0,0,255)
		surface.DrawRect(bar2xpos+12+section*5,bar2ypos+2,section-4,16)
		
		-- Slider
		--local bar2 = alertdanger*0.4207
		local bar2 = alertdanger*0.375
		draw.RoundedBox(4,bar2xpos+3+bar2,bar2ypos-2,10,24,Color(255,255,255,255))
	end
	
	if IsValid(LocalPlayer():GetNWEntity("sm_SpecRagdoll")) then
		local halfW, halfH = ScrW()/2, ScrH()/2
		local halfHsin = halfH + math.sin(CurTime()*32) * 4
		draw.RoundedBox( 8, halfW-38, halfH-32, 76, 76, Color(128,128,128,128) )
		draw.RoundedBox( 8, halfW-32, halfHsin-32, 64, 64, Color(255,255,255,128) )
		draw.SimpleText( "E", "ContentHeader", halfW, halfHsin, Color(0,0,0,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end
end)


local function IsVisible(ply,npc)
	local tr = util.TraceLine{
		start = npc:EyePos(),
		endpos = ply:EyePos(),
		filter = {ply,npc},
		mask = MASK_VISIBLE_AND_NPCS
	}
	
	return not tr.Hit
end

local function CLcheckLOS(npc)
	if cvars.Bool("ai_disabled") or cvars.Bool("ai_ignoreplayers") then return -1 end
	-- Variables
	local minsight = cvars.Number("stealth_minsight")
	local maxsight = cvars.Number("stealth_maxsight")
	local movementbonus = cvars.Number("stealth_movebonus")
	local multiplier = cvars.Number("stealth_multiplier")
	if minsight < 0 then minsight = 0 end
	if maxsight < 0 then maxsight = 0 end
	if movementbonus < 0 then movementbonus = 0 end
	if multiplier < 0 then multiplier = 0 end
	--
	
	local sightrange = 0

	local isvisible = IsVisible(LocalPlayer(),npc)
	local eyesang = nil
	local yawdiff = math.abs(math.AngleDifference(npc:EyeAngles().y, (LocalPlayer():GetPos()-npc:GetPos()):Angle().y))
	--local eyesobj = npc:LookupAttachment( "eyes" )
	--if eyesobj then eyesang = npc:GetAttachment( eyesobj ) end
	--if eyesang then yawdiff = math.abs(math.AngleDifference(eyesang["Ang"].y,(LocalPlayer():GetPos()-npc:GetPos()):Angle().y))
	--else yawdiff = math.abs(math.AngleDifference(npc:GetAngles().y,(LocalPlayer():GetPos()-npc:GetPos()):Angle().y)) end
	local lightbonus = ((maxsight - minsight) * (luminocity/255))
	local playerspeed = LocalPlayer():GetVelocity():Length()
	--local playerdist = LocalPlayer():GetPos():DistToSqr(npc:GetPos())
	sightrange = ( minsight + lightbonus * (1+(movementbonus*(playerspeed/200))) ) * multiplier
	
	if yawdiff>60 or not isvisible then
		return -1
	end
	local wep = LocalPlayer():GetActiveWeapon()
	if IsValid(wep) then
		if wep:GetClass() == "weapon_cbox" and LocalPlayer():Crouching() and LocalPlayer():GetVelocity():LengthSqr() < 32 then
			return -1
		end
	end
	--
	
	--if LocalPlayer():Crouching() then sightrange = sightrange/2 end
	--if not isvisible then LocalPlayer().shooting = LocalPlayer().shooting/2 end
	
	return math.Clamp((  255 - math.Clamp(255 * ((LocalPlayer():GetPos():DistToSqr(npc:GetPos())-(sightrange*sightrange * 0.75)) / math.Max((sightrange*sightrange * 0.75),1000)),0,255))  , 0 , 255)
end

local propcolors = {
	["solidmetal"] = Vector(80, 80, 80),
	["Metal_Box"] = Vector(80, 80, 80),
	["metal"] = Vector(80, 80, 80),
	["metal_bouncy"] = Vector(80, 80, 80),
	["slipperymetal"] = Vector(80, 80, 80),
	["metalgrate"] = Vector(80, 80, 80),
	["metalvent"] = Vector(90, 90, 90),
	["metalpanel"] = Vector(80, 80, 80),
	["dirt"] = Vector(90, 75, 60),
	["mud"] = Vector(90, 75, 60),
	["slipperyslime"] = Vector(200, 0, 0),
	["grass"] = Vector(50, 60, 30),
	--["tile"] = Vector(150, 110, 90),
	["wood"] = Vector(110, 90, 70),
	["wood_lowdensity"] = Vector(110, 90, 70),
	["wood_box"] = Vector(110, 90, 70),
	["wood_crate"] = Vector(110, 90, 70),
	["wood_plank"] = Vector(110, 90, 70),
	["wood_solid"] = Vector(110, 90, 70),
	["wood_panel"] = Vector(110, 90, 70),
	--["water"] = Vector(64, 100, 120),
	["slime"] = Vector(60, 60, 30),
	["quicksand"] = Vector(90, 75, 60),
	--["wade"] = Vector(64, 100, 120),
	--["ladder"] = Vector(80, 80, 80),
	--["woodladder"] = Vector(110, 90, 70),
	--["glass"] = Vector(255, 0, 255),
	--["computer"] = Vector(80, 80, 80),
	["cardboard"] = Vector(160, 130, 100),
	["concrete"] = Vector(100, 100, 100),
	["rock"] = Vector(100, 100, 100),
	["stone"] = Vector(100, 100, 100),
	["porcelain"] = Vector(100, 100, 100),
	["boulder"] = Vector(100, 100, 100),
	["gravel"] = Vector(100, 100, 90),
	["brick"] = Vector(150, 110, 90),
	["concrete_block"] = Vector(100, 100, 100),
	["chainlink"] = Vector(80, 80, 80),
	["chain"] = Vector(80, 80, 80),
	["flesh"] = Vector(180, 130, 90),
	["bloodyflesh"] = Vector(180, 130, 90),
	["alienflesh"] = Vector(210, 210, 90),
	["armorflesh"] = Vector(180, 130, 90),
	["watermelon"] = Vector(60, 60, 40),
	["snow"] = Vector(200, 200, 220),
	["ice"] = Vector(200, 200, 220),
	["sand"] = Vector(230, 200, 150),
	["foliage"] = Vector(60, 60, 40),
}
hook.Add("Think","SendLuminocity",function() --Keep checking what npcs are alert to
	if CurTime()+.5>lastsend then
		lastsend=CurTime()
		isnpc = GetGlobalBool("BadNPCOnMap", false)
		--local complight = math.max((render.ComputeLighting(LocalPlayer():GetPos()+Vector(0,0,40),Vector(0,0,-1))*Vector(255,255,255)):Length(),(render.ComputeLighting(LocalPlayer():GetPos()+Vector(0,0,40),Vector(0,0,1))*Vector(255,255,255)):Length(),(render.ComputeLighting(LocalPlayer():GetPos()+Vector(0,0,40),Vector(0,1,0))*Vector(255,255,255)):Length(),(render.ComputeLighting(LocalPlayer():GetPos()+Vector(0,0,40),Vector(0,-1,0))*Vector(255,255,255)):Length(),(render.ComputeLighting(LocalPlayer():GetPos()+Vector(0,0,40),Vector(1,0,0))*Vector(255,255,255)):Length(),(render.ComputeLighting(LocalPlayer():GetPos()+Vector(0,0,40),Vector(-1,0,0))*Vector(255,255,255)):Length())
		--local complight = math.max((render.ComputeLighting(LocalPlayer():GetPos(),Vector(0,0,-1))*Vector(255,255,255)):Length(),(render.ComputeLighting(LocalPlayer():GetPos(),Vector(0,0,1))*Vector(255,255,255)):Length())
		--luminocity = math.Clamp(complight/2.8 + ((LocalPlayer():FlashlightIsOn() and 1 or 0)*100),0,255)
		-- Back to the old method
		
		local mattrace = util.QuickTrace( LocalPlayer():WorldSpaceCenter(), Vector(0,0,-64), LocalPlayer() )
		local floorcol = propcolors[util.GetSurfacePropName(mattrace.SurfaceProps)] or Vector(255, 0, 255)
		local plycol = LocalPlayer():GetPlayerColor() * LocalPlayer():GetColor().a
		
		local diff = Vector(math.abs(floorcol[1] - plycol[1]), math.abs(floorcol[2] - plycol[2]), math.abs(floorcol[3] - plycol[3]))
		
		luminocity = math.Clamp((math.max(diff[1], diff[2], diff[3]) + 30) * render.GetLightColor(LocalPlayer():GetPos()):LengthSqr()*2 + (!(LocalPlayer():Crouching() or (LocalPlayer().IsProne and LocalPlayer():IsProne())) and 1 or 0) * 50 + (LocalPlayer():FlashlightIsOn() and 1 or 0)*100, 0, 255)
		--luminocity = math.Clamp((render.GetLightColor(LocalPlayer():GetPos())*Vector(255,255,255)):Length()+ ((LocalPlayer():FlashlightIsOn() and 1 or 0)*100),0,255)
		
		RunConsoleCommand("p_sendluminocity",tostring(luminocity))
		stealthmod.luminocity = luminocity
		
		-- Sight Range Calculation
		if !table.IsEmpty(npctable) then
			alertdanger = 0
			dangernpc = nil
			for k,v in pairs(npctable) do
				if IsValid(v) then 
					local aux = CLcheckLOS(v)
					if aux >= alertdanger then
						alertdanger = aux
						dangernpc = v
					end
				else
					table.RemoveByValue(npctable,v)
				end
			end
			stealthmod.alertdanger = alertdanger
		end
	end
	-- Music
	if !alertmusic then alertmusic = CreateSound( LocalPlayer() , "#stealth/alertmusic.wav") end
	if alertmusic:IsPlaying() then
		if insight == 1 and LocalPlayer():Alive() then
			if timer.Exists("musicturnoff") then timer.Remove("musicturnoff") end
			alertmusic:ChangeVolume( 1 , 0 ) 
			if cvars.Number("stealth_alerttime") > 2 then
				timer.Create( "musicturnoff", cvars.Number("stealth_alerttime")-2, 1, function() alertmusic:ChangeVolume( 0.01, 2 ) end) 
			end
		elseif table.IsEmpty(hunters) and fadingmusic == 0 then
			fadingmusic = 1
			alertmusic:ChangeVolume( 0.01, 2 )
			timer.Simple( 2, function()
				if table.IsEmpty(hunters) then
					alertmusic:Stop()
					playingmusic = 0
				else
					alertmusic:ChangeVolume( 1 , 0 )
				end
				fadingmusic = 0
			end) 
		end
	end
	
	-- Amnesia-like dark vision, made by AaronTheSnob
	-- Cool stuff, but not related to this mod
	--[[
	if GetConVar("stealth_enabledarkvision"):GetInt() != 0 then
		-- Check if the player is in darkness.
		if (luminocity < 10) and (!LocalPlayer():FlashlightIsOn()) then
			-- Make the light fade in.
			darkvisionbrightness = darkvisionbrightness + 0.3 * FrameTime()
			if darkvisionbrightness >= 1 then
				darkvisionbrightness = 1
			end
		else
			-- Make the light fade out
			darkvisionbrightness = darkvisionbrightness - 1 * FrameTime()
			if darkvisionbrightness <= 0 then
				darkvisionbrightness = 0
			end
		end
	else
		-- Turn off the light
		darkvisionbrightness = 0
	end
	
	-- Apply the light settings
	if ( dlight ) then
		dlight.pos = LocalPlayer():GetShootPos()
		-- Toned down the brightness a bit
		dlight.r = 5 * darkvisionbrightness
		dlight.g = 5 * darkvisionbrightness
		dlight.b = 15 * darkvisionbrightness
		dlight.brightness = 1
		dlight.Decay = 1000
		dlight.Size = 512
		dlight.DieTime = CurTime() + 1
	end
	]]--
end)

local function NPCAlerted()
	local npc = net.ReadEntity()
	local silent = net.ReadBool()
	if IsValid( npc ) and !table.HasValue(hunters, npc) then
	--if IsValid( npc ) and !IsValid(hunters[npc:EntIndex()]) then
		table.insert(hunters, npc)
		--table.insert(hunters, npc:EntIndex(), npc)
		insight = 1
		
		if cvars.Bool("stealth_enablesound") and !silent and sounddelay < CurTime() then
			npc:EmitSound( "stealth/alert.wav" , 100, 100, 1.0, CHAN_AUTO )
			sounddelay = CurTime() + 1
		end
		timer.Simple(cvars.Number("stealth_backuptime"), function() if !alertmusic:IsPlaying() and !table.IsEmpty(hunters) and cvars.Bool("stealth_enablemusic") then
			alertmusic:Play()
			playingmusic = 1
		end
		end)
	end
end

net.Receive( "NPCAlerted", NPCAlerted )

local function NPCCalmed()
	local npc = net.ReadEntity()
	if IsValid( npc ) and table.HasValue(hunters, npc) then
	--if IsValid( npc ) and IsValid(hunters[npc:EntIndex()]) then
		table.RemoveByValue(hunters, npc)
		--table.remove(hunters, npc:EntIndex())
	end
end

net.Receive( "NPCCalmed", NPCCalmed )

local function NPCCreateEffect(npc, effectname)
	if !IsValid(npc) then return end
	local allents = ents.GetAll()
	if cvars.Bool("stealth_enablefx") then
		local effect = EffectData()
		effect:SetOrigin(npc:GetPos())
		effect:SetEntity(npc)
		util.Effect( effectname, effect, true, true )
	end
	local e = ents.GetAll()[#allents +1]
	if e then return e end
end

local function NPCEffect()
	local npc = net.ReadEntity()
	local effectname = net.ReadString()
	if IsValid(npc.currenteffect) then 
		npc.currenteffect:DoRemove()
	end
	npc.currenteffect = NPCCreateEffect(npc,effectname)
end

net.Receive( "NPCEffect", NPCEffect )

timer.Create("CheckVisible",.5,0,function() --Keep checking what alerted npcs are visible
	if table.IsEmpty(hunters) then return end
	insight = 0
	--for k = #hunters, 1, -1 do
	for k, v in pairs(hunters) do
		--if IsValid(hunters[k]) then
		if IsValid(v) then
			--if insight == 0 and IsVisible(LocalPlayer(),hunters[k]) then
			if insight == 0 and IsVisible(LocalPlayer(),v) then
				insight = 1
			end
		else
			table.remove(hunters, k)
		end
	end
end)

hook.Add("PopulateToolMenu", "stealthmenu", function()
	spawnmenu.AddToolMenuOption("Options", "Stealth Mod", "stealthmodclient", "Client Settings", "", "", SettingsPanelClient)
	spawnmenu.AddToolMenuOption("Options", "Stealth Mod", "stealthmodserver", "Server Settings", "", "", SettingsPanelServer)
end)

function SettingsPanelClient(panel)

	local pan = {}
	
	local p = panel:AddControl("Slider", {
		Label = "HUD Mode",
		Min = "0",
		Max = "4"
	})
	p:SetValue( cvars.Number( "stealth_drawbar" ) )
	p.OnValueChanged = function(self)
		RunConsoleCommand("stealth_drawbar", math.Round(self:GetValue(),0))
	end
	table.insert(pan,{"stealth_drawbar",p,1})
		
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Enable Effects"
	})
	p:SetValue( cvars.Number( "stealth_enablefx" ) )
	p.OnChange = function(self)
		RunConsoleCommand("stealth_enablefx", self:GetChecked() and 1 or 0)
	end
	table.insert(pan,{"stealth_enablefx",p,1})
	
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Enable Sound"
	})
	p:SetValue( cvars.Number( "stealth_enablesound" ) )
	p.OnChange = function(self)
		RunConsoleCommand("stealth_enablesound", self:GetChecked() and 1 or 0)
	end
	table.insert(pan,{"stealth_enablesound",p,1})
	
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Enable Music"
	})
	p:SetValue( cvars.Number( "stealth_enablemusic" ) )
	p.OnChange = function(self)
		RunConsoleCommand("stealth_enablemusic", self:GetChecked() and 1 or 0)
	end
	table.insert(pan,{"stealth_enablemusic",p,1})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Hud X Pos",
		Min = "0",
		Max = "4000"
	})
	p:SetValue( cvars.Number( "stealth_hudx" ) )
	p.OnValueChanged = function(self)
		RunConsoleCommand("stealth_hudx", math.Round(self:GetValue()))	
	end
	table.insert(pan,{"stealth_hudx",p,29})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Hud Y Pos",
		Min = "0",
		Max = "3000"
	})
	p:SetValue( cvars.Number( "stealth_hudy" ) )
	p.OnValueChanged = function(self)
		RunConsoleCommand("stealth_hudy", math.Round(self:GetValue()))	
	end
	table.insert(pan,{"stealth_hudy",p,220})
	
	
	
	local p = panel:AddControl("Button", {
		Label = "Default settings",
		Command = ""
	})
	p.DoClick = function()
		for k,v in pairs(pan) do
			v[2]:SetValue(v[3])
		end
		--[[
		pan[1][2]:SetValue(1)
		pan[2][2]:SetValue(1)
		pan[3][2]:SetValue(1)
		pan[4][2]:SetValue(1)
		pan[5][2]:SetValue(29)
		pan[6][2]:SetValue(220)	
		]]--
	end
	
end

function SettingsPanelServer(panel)

	local pan = {}

	local p = panel:AddControl("CheckBox", {
		Label = "Enable Stealth"
	})
	p:SetValue( cvars.Bool( "stealth_enabled" ) )
	p.OnChange = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_enabled")
			net.WriteFloat(self:GetChecked() and 1 or 0)
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_enabled",p,1})
	
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Relationship Override"
	})
	p:SetValue( cvars.Number( "stealth_override" ) )
	p.OnChange = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_override")
			net.WriteFloat(self:GetChecked() and 1 or 0)
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_override",p,1})
	
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Enable Stunning Players"
	})
	p:SetValue( cvars.Bool( "stealth_stunplayers" ) )
	p.OnChange = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_stunplayers")
			net.WriteFloat(self:GetChecked() and 1 or 0)
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_stunplayers",p,1})
	
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Keep Corpses Override"
	})
	p:SetValue( cvars.Bool( "stealth_keepcorpses" ) )
	p.OnChange = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_keepcorpses")
			net.WriteFloat(self:GetChecked() and 1 or 0)
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_keepcorpses",p,0})
	
	
	
	local p = panel:AddControl("CheckBox", {
		Label = "Alert on damage"
	})
	p:SetValue( cvars.Bool( "stealth_alertondamage" ) )
	p.OnChange = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_alertondamage")
			net.WriteFloat(self:GetChecked() and 1 or 0)
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_alertondamage",p,1})

	
	
	local p = panel:AddControl("Slider", {
		Label = "Alert Time",
		Type = "Float",
		Min = "0",
		Max = "100"
	})
	p:SetValue( cvars.Number( "stealth_alerttime" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_alerttime")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_alerttime",p,15})

	panel:AddControl("Label", {
		Text = "Time until NPCs leave their alerted state."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Difficulty Multiplier",
		Type = "Float",
		Min = "0",
		Max = "5"
	})
	p:SetValue( cvars.Number( "stealth_multiplier" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_multiplier")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_multiplier",p,1})

	
	
	panel:AddControl("Label", {
		Text = "This increases or decreases all stealth settings."
	})
	
	local p = panel:AddControl("Slider", {
		Label = "Min Sight Range",
		Type = "Float",
		Min = "0",
		Max = "5000"
	})
	p:SetValue( cvars.Number( "stealth_minsight" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_minsight")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_minsight",p,200})

	
	
	panel:AddControl("Label", {
		Text = "Range at which an NPC can detect a standing player with minimum light level."
	})
	
	local p = panel:AddControl("Slider", {
		Label = "Max Sight Range",
		Type = "Float",
		Min = "0",
		Max = "5000"
	})
	p:SetValue( cvars.Number( "stealth_maxsight" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_maxsight")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_maxsight",p,1500})

	panel:AddControl("Label", {
		Text = "Range at which an NPC can detect a standing player with maximum light level."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Movement Bonus",
		Type = "Float",
		Min = "0",
		Max = "5"
	})
	p:SetValue( cvars.Number( "stealth_movebonus" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_movebonus")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_movebonus",p,0.3})

	panel:AddControl("Label", {
		Text = "Sight range will be increased this much when a player is running (double if sprinting)."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Min Hearing Range",
		Type = "Float",
		Min = "0",
		Max = "5000"
	})
	p:SetValue( cvars.Number( "stealth_minhearing" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_minhearing")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_minhearing",p,200})

	panel:AddControl("Label", {
		Text = "Range at which an NPC can hear a running player (double if sprinting)."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Shot Range",
		Type = "Float",
		Min = "0",
		Max = "5000"
	})
	p:SetValue( cvars.Number( "stealth_shotrange" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_shotrange")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_shotrange",p,1000})

	panel:AddControl("Label", {
		Text = "Range at which an NPC can hear a gunshot."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Suppression Multiplier",
		Type = "Float",
		Min = "0",
		Max = "1"
	})
	p:SetValue( cvars.Number( "stealth_suppmultiplier" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_suppmultiplier")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_suppmultiplier",p,0.3})

	panel:AddControl("Label", {
		Text = "Hearing range multiplier for suppressed weapons."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Backup time",
		Type = "Float",
		Min = "0",
		Max = "100"
	})
	p:SetValue( cvars.Number( "stealth_backuptime" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_backuptime")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_backuptime",p,0.2})

	panel:AddControl("Label", {
		Text = "Time in seconds until an enemy alerts nearby npcs after detecting a player."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Backup range",
		Type = "Float",
		Min = "0",
		Max = "5000"
	})
	p:SetValue( cvars.Number( "stealth_backuprange" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_backuprange")
			net.WriteFloat(self:GetValue())
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_backuprange",p,800})

	panel:AddControl("Label", {
		Text = "When an enemy detects you, he will alert all enemies inside this range."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Max corpses",
		Min = "0",
		Max = "100"
	})
	p:SetValue( cvars.Number( "stealth_maxcorpses" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_maxcorpses")
			net.WriteFloat(math.Round(self:GetValue()))
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_maxcorpses",p,10})

	panel:AddControl("Label", {
		Text = "Limits the maximum amount of enemy corpses (only for NPCs affected by this mod)."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Sensor Time",
		Min = "0",
		Max = "100"
	})
	p:SetValue( cvars.Number( "stealth_sensortime" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_sensortime")
			net.WriteFloat(math.Round(self:GetValue()))
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_sensortime",p,10})

	panel:AddControl("Label", {
		Text = "Time until the Proximity Sensor deactivates."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Sensor Range",
		Min = "0",
		Max = "5000"
	})
	p:SetValue( cvars.Number( "stealth_sensorrange" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_sensorrange")
			net.WriteFloat(math.Round(self:GetValue()))
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_sensorrange",p,500})

	panel:AddControl("Label", {
		Text = "Detection range for the Proximity Sensor."
	})
	
	
	
	local p = panel:AddControl("Slider", {
		Label = "Sleep Time",
		Min = "0",
		Max = "1000"
	})
	p:SetValue( cvars.Number( "stealth_sleeptime" ) )
	p.OnValueChanged = function(self)
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_sleeptime")
			net.WriteFloat(math.Round(self:GetValue()))
			net.SendToServer()
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to change this option.")
			chat.PlaySound()
		end			
	end
	table.insert(pan,{"stealth_sleeptime",p,60})

	panel:AddControl("Label", {
		Text = "Time in seconds until an unconcious enemy wakes up."
	})
	
	
	
	local p = panel:AddControl("Button", {
		Label = "Reload lists",
		Command = ""
	})
	p.DoClick = function() 
		if LocalPlayer():IsSuperAdmin() then
			net.Start("stealth_reloadsettings")
			net.SendToServer() 
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to do this.")
			chat.PlaySound()
		end			
	end
	
	local p = panel:AddControl("Button", {
		Label = "Default settings",
		Command = ""
	})
	p.DoClick = function() 
		if LocalPlayer():IsSuperAdmin() then
			-- Reset panel
			-- GetDefault() doesn't work for some reason, so...
			for k,v in pairs(pan) do
				v[2]:SetValue(v[3])
			end
			--[[
			pan[1][2]:SetValue(1)
			pan[2][2]:SetValue(1)
			pan[3][2]:SetValue(0)
			pan[4][2]:SetValue(1)
			pan[5][2]:SetValue(15)
			pan[6][2]:SetValue(1)
			pan[7][2]:SetValue(200)
			pan[8][2]:SetValue(1500)
			pan[9][2]:SetValue(0.3)
			pan[10][2]:SetValue(200)
			pan[11][2]:SetValue(1000)
			pan[12][2]:SetValue(0.3)
			pan[13][2]:SetValue(2)
			pan[14][2]:SetValue(800)
			pan[15][2]:SetValue(10)
			pan[16][2]:SetValue(10)
			pan[17][2]:SetValue(10)
			pan[18][2]:SetValue(10)
			]]--
		else
			chat.AddText(Color(255,62,62), "WARNING: ", Color(255,255,255), "You must be a super-admin to do this.")
			chat.PlaySound()
		end			
	end
end
